-------------------------------------------------------------------------------------------------------------------
Theme: The more you have, the worse it is
-------------------------------------------------------------------------------------------------------------------

Game Name: 

-------------------------------------------------------------------------------------------------------------------

Idea:
- Base builder on the side of a mountain
- Mine (down) into the mountain to get resources 
	Stone
	Iron
	Coal maybe
	Gold
	Gems
- Maybe also get wood from trees as a starting resources
- Could also need to farm for food
- As you mine, there is a chance that you will get corruption
- Corruption will cause attacks to be made against your base from the (top) of the map
- Assign workers to different job types (or get workers of different types)
	Farmers - Farm to get food
	Miners - Mine to get resoures
	Buildder - Make buildings and defences
	Fighters - Deffend the base from the attacks made against you
- Research tech using gold and gems
	Tech unlocks new buildings and improves current ones
	To win the game you need to progress an anti corruption tech chain
		The final two levels of this will first stop the production of corruption and then reduce how much you current have
		The second thing there will need buildings to be made to reduce it
			While corruption is being reduces you will still be attacked as if your corruption was increacing to keep the difficulty of the game

-------------------------------------------------------------------------------------------------------------------

Done:
- Map display
- Map generation
	Mountain edge
	Randonly placed trees (and this could also be other things like rocks)
	Map generates resources in the mountain and underground
- Entity display
- Job system
- Resource types
	Currently there is no maximum storage capacity
- Buildings
- Tech
- Enemies are a thing
	
-------------------------------------------------------------------------------------------------------------------

To Do:	
- More enemies
- Woker animation

-------------------------------------------------------------------------------------------------------------------

Could Do
- Items be individual objects that need to be moved arround
- Music
- Sound Fx
- More likely to get high teir resources, the more you dig into the mountain
- Animated buildings
- Turrets

-------------------------------------------------------------------------------------------------------------------

Fixed Bugs:
- Fixed blacksmiths not standing on the correct tile when crafting iron
- Fixed entities spawning while there is an attack
- Fixed not being able to lose and just being stuck with loads of enemies moving around

-------------------------------------------------------------------------------------------------------------------

Bugs:
- Chunks dont always render in the correct order, so the top half of trees will sometimes be cut off
	This results in a very clear horizontal line through the trees as it happens as  specific y values
	If I have sprites wider than one tile ill probably get the same issue going vertical
- Workers can be thrown onto a tile that isn't walkable, sometimes this means that they can't move after
- Monsters can get stuck and not move in certain situations
	
-------------------------------------------------------------------------------------------------------------------

Music
-

-------------------------------------------------------------------------------------------------------------------