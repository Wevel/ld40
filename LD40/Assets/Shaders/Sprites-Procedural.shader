// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Sprites/Procedural"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
		ZWrite Off
        //ZWrite On
		//ZTest LEqual
        Blend One OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            #include "UnityCG.cginc"

			#ifdef UNITY_INSTANCING_ENABLED

			UNITY_INSTANCING_CBUFFER_START(PerDrawSprite)
				// SpriteRenderer.Color while Non-Batched/Instanced.
				fixed4 unity_SpriteRendererColorArray[UNITY_INSTANCED_ARRAY_SIZE];
				// this could be smaller but that's how bit each entry is regardless of type
				float4 unity_SpriteFlipArray[UNITY_INSTANCED_ARRAY_SIZE];
			UNITY_INSTANCING_CBUFFER_END

			#define _RendererColor unity_SpriteRendererColorArray[unity_InstanceID]
			#define _Flip unity_SpriteFlipArray[unity_InstanceID]

			#endif // instancing

			CBUFFER_START(UnityPerDrawSprite)
			#ifndef UNITY_INSTANCING_ENABLED
				fixed4 _RendererColor;
				float4 _Flip;
			#endif
				float _EnableExternalAlpha;
			CBUFFER_END

			// Material Color.
			fixed4 _Color;

			struct Point
			{
				 float3 vertex;
				 float4 color;
				 float2 uv;
			 };      
 
			 StructuredBuffer<Point> points;

			//struct appdata_t
			//{
			//	float4 vertex   : POSITION;
			//	float4 color    : COLOR;
			//	float2 texcoord : TEXCOORD0;
			//	UNITY_VERTEX_INPUT_INSTANCE_ID
			//};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
			};
			
			v2f vert (uint id : SV_VertexID, uint inst : SV_InstanceID)
			{
				v2f OUT;

				float4 vertex =  float4(points[id].vertex, 1.0f);
				float4 col = points[id].color;

				//UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);

			#ifdef UNITY_INSTANCING_ENABLED
				vertex.xy *= _Flip.xy;
			#endif

				OUT.vertex = UnityObjectToClipPos(vertex);
				OUT.texcoord = points[id].uv;
				OUT.color = col * _Color * _RendererColor;

				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _AlphaTex;

			fixed4 SampleSpriteTexture (float2 uv)
			{
				fixed4 color = tex2D (_MainTex, uv);

			#if ETC1_EXTERNAL_ALPHA
				fixed4 alpha = tex2D (_AlphaTex, uv);
				color.a = lerp (color.a, alpha.r, _EnableExternalAlpha);
			#endif

				return color;
			}

			void frag(v2f IN : SV_Target, out float4 c : COLOR)//, inout float depth : DEPTH)
			{
				c = SampleSpriteTexture (IN.texcoord) * IN.color;
				c.rgb *= c.a;
				//if (c.a < 1) depth = 1;
				//else depth = 0;
				//depth = 1 - (IN.vertex.z * c.a);
			}
			ENDCG
        }
    }
}
