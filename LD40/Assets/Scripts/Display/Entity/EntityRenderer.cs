﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityRenderer : MonoBehaviour 
{
	private SpriteRenderer sr;

	public float zOffset;

	public Entity currentEntity { get; private set; }
	private Vector2 lastPosition;

	private void Awake ()
	{
		sr = this.GetComponent<SpriteRenderer> ();
	}

	private void Update ()
	{
		if (currentEntity != null)
		{
			transform.position = new Vector3 (currentEntity.position.x, currentEntity.position.y, zOffset) + currentEntity.type.displayOffset;

			if(transform.position.x != lastPosition.x) sr.flipX = transform.position.x < lastPosition.x;

			//ToDo: Animation based on current task

			lastPosition = transform.position;
		}
	}

	public void Show (Entity entity)
	{
		currentEntity = entity;
		transform.position = new Vector3 (currentEntity.position.x, currentEntity.position.y, zOffset) + currentEntity.type.displayOffset;
		sr.sprite = entity.type.display;
		gameObject.SetActive (true);
	}

	public void Hide ()
	{
		currentEntity = null;
		gameObject.SetActive (false);
	}
}
