﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class MapChunkRenderer : System.IDisposable
{
	private class DetailMaterial
	{
		public int x;
		public int y;

		public Dictionary<int, MaterialType> materials = new Dictionary<int, MaterialType> ();

		public int detailsCount = 0;

		public DetailMaterial (int x, int y)
		{
			this.x = x;
			this.y = y;
		}
	}

	private struct MaterialType
	{
		public Material material;
		public ComputeBuffer buffer;
		public Texture2D texture;

		public MaterialType (Material material, Texture2D texture, int pointsCount)
		{
			this.material = material;
			this.texture = texture;
			this.buffer = new ComputeBuffer (pointsCount, Marshal.SizeOf (typeof (Point)), ComputeBufferType.Append);
		}

		public void Dispose ()
		{
			if (material != null) Object.Destroy (material);
			buffer?.Dispose ();
		}
	}

	[StructLayout (LayoutKind.Sequential)]
	private struct Point
	{
		public Vector3 vertex;
		public Vector4 colour;
		public Vector2 uv;
	}

	public readonly Vector4 whiteColour = new Vector4 (1, 1, 1, 1);
	public readonly Vector4 blueprintTint = new Vector4 (1, 1, 1, 0.5f);
	public readonly Vector4 clearColour = new Vector4 (0, 0, 0, 0);

	public readonly int width;
	public readonly int height;

	public int chunkX { get; private set; }
	public int chunkY { get; private set; }
	public int baseX { get; private set; }
	public int baseY { get; private set; }

	private readonly int pointsCount;

	private Point[] tilePoints;
	private Point[] clearedTilePoints;

	private Dictionary<int, MaterialType> tileMaterials = new Dictionary<int, MaterialType> ();
	private Dictionary<int, MaterialType> buildingMaterials = new Dictionary<int, MaterialType> ();
	private List<DetailMaterial> detailMaterials = new List<DetailMaterial> ();
	private Queue<MaterialType> materialTypePool = new Queue<MaterialType> ();

	public MapChunkRenderer (int size)
	{
		this.baseX = baseX;
		this.baseY = baseY;
		this.width = size;
		this.height = size;

		tilePoints = new Point[6];
		clearedTilePoints = new Point[6];

		for (int i = 0; i < tilePoints.Length; i++)
		{
			tilePoints[i] = new Point ();
			tilePoints[i].colour = whiteColour;

			clearedTilePoints[i] = new Point ();
			clearedTilePoints[i].colour = clearColour;
		}

		pointsCount = width * height * tilePoints.Length;
	}

	public void AddMaterial (int materialIndex, Material material, Texture2D texture, bool clearOldData = true)
	{
		MaterialType type;
		if (tileMaterials.TryGetValue (materialIndex, out type))
		{
			// Destroy the old material if there is one
			if (type.material != null) Object.Destroy (type.material);

			// There is already a material with this index, so we need to reset the material and then clear the buffer if needed
			type.material = new Material (material);
			type.material.mainTexture = texture;
			if (clearOldData) clearMaterialBuffer (type);
		}
		else
		{
			tileMaterials.Add (materialIndex, getMaterialType (material, texture));
		}

		if (buildingMaterials.TryGetValue (materialIndex, out type))
		{
			// Destroy the old material if there is one
			if (type.material != null) Object.Destroy (type.material);

			// There is already a material with this index, so we need to reset the material and then clear the buffer if needed
			type.material = new Material (material);
			type.material.mainTexture = texture;
			if (clearOldData) clearMaterialBuffer (type);
		}
		else
		{
			buildingMaterials.Add (materialIndex, getMaterialType (material, texture));
		}
	}

	public void Setup (int chunkX, int chunkY)
	{
		this.chunkX = chunkX;
		this.chunkY = chunkY;
		this.baseX = chunkX * width;
		this.baseY = chunkY * height;

		// Clear out any existing materials if they have been set
		foreach (KeyValuePair<int, MaterialType> item in tileMaterials) materialTypePool.Enqueue (item.Value);
		tileMaterials.Clear ();

		foreach (KeyValuePair<int, MaterialType> item in buildingMaterials) materialTypePool.Enqueue (item.Value);
		buildingMaterials.Clear ();

		for (int i = 0; i < detailMaterials.Count; i++)
		{
			foreach (KeyValuePair<int, MaterialType> item in detailMaterials[i].materials) materialTypePool.Enqueue (item.Value);
		}

		detailMaterials.Clear ();
	}

	public void RenderChunk ()
	{
		if (tileMaterials != null)
		{
			foreach (MaterialType item in tileMaterials.Values)
			{
				for (int i = 0; i < item.material.passCount; i++)
				{
					item.material.SetPass (i);
					Graphics.DrawProcedural (MeshTopology.Triangles, pointsCount);
				}
			}
		}

		if (buildingMaterials != null)
		{
			foreach (MaterialType item in buildingMaterials.Values)
			{
				for (int i = 0; i < item.material.passCount; i++)
				{
					item.material.SetPass (i);
					Graphics.DrawProcedural (MeshTopology.Triangles, pointsCount);
				}
			}
		}

		if (detailMaterials != null)
		{
			for (int i = 0; i < detailMaterials.Count; i++)
			{
				foreach (KeyValuePair<int, MaterialType> item in detailMaterials[i].materials)
				{
					for (int p = 0; p < item.Value.material.passCount; p++)
					{
						item.Value.material.SetPass (p);
						Graphics.DrawProcedural (MeshTopology.Triangles, pointsCount);
					}
				}
			}
		}
	}

	public void Dispose ()
	{
		if (tileMaterials != null && tileMaterials.Count > 0)
		{
			foreach (KeyValuePair<int, MaterialType> item in tileMaterials) item.Value.Dispose ();
		}

		if (buildingMaterials != null && buildingMaterials.Count > 0)
		{
			foreach (KeyValuePair<int, MaterialType> item in buildingMaterials) item.Value.Dispose ();
		}

		if (detailMaterials != null && detailMaterials.Count > 0)
		{
			for (int i = 0; i < detailMaterials.Count; i++)
			{
				foreach (KeyValuePair<int, MaterialType> item in detailMaterials[i].materials) item.Value.Dispose ();
			}
		}

		if (materialTypePool != null && materialTypePool.Count > 0)
		{
			while (materialTypePool.Count > 0) materialTypePool.Dequeue ().Dispose ();
		}
	}

	internal void SetTile (int tx, int ty, MapRenderer.TileRendererType tileType, MapRenderer.TileRendererType[] detailTypes, MapRenderer.TileRendererType buildingTileType, bool buildingIsBlueprint)
	{
		if (!InInChunk (tx, ty))
		{
			Debug.LogError ("MapChunkRenderer.SetTile: Tile is not in the chunk");
			return;
		}

		int x = tx - baseX;
		int y = ty - baseY;

		if (tileType != null)
		{
			MaterialType materialType;
			if (tileMaterials.TryGetValue (tileType.materialIndex, out materialType))
			{
				setupPointsArray (x, y, tileType, whiteColour);
				materialType.buffer.SetData (tilePoints, 0, (x + ((height - y - 1) * width)) * tilePoints.Length, tilePoints.Length);
				materialType.material.SetBuffer ("points", materialType.buffer);
			}
			else
			{
				Debug.LogError ($"MapChunkRenderer.SetTile: No material with index {tileType.materialIndex}");
			}
		}
		else
		{
			foreach (KeyValuePair<int, MaterialType> item in tileMaterials)
			{
				item.Value.buffer.SetData (clearedTilePoints, 0, (x + (y * width)) * clearedTilePoints.Length, clearedTilePoints.Length);
				item.Value.material.SetBuffer ("points", item.Value.buffer);
			}
		}

		if (buildingTileType != null)
		{
			MaterialType materialType;
			if (buildingMaterials.TryGetValue (buildingTileType.materialIndex, out materialType))
			{
				if (buildingIsBlueprint) setupPointsArray (x, y, buildingTileType, blueprintTint);
				else setupPointsArray (x, y, buildingTileType, whiteColour);
				materialType.buffer.SetData (tilePoints, 0, (x + ((height - y - 1) * width)) * tilePoints.Length, tilePoints.Length);
				materialType.material.SetBuffer ("points", materialType.buffer);
			}
			else
			{
				Debug.LogError ($"MapChunkRenderer.SetTile: No material with index {buildingTileType.materialIndex}");
			}
		}
		else
		{
			foreach (KeyValuePair<int, MaterialType> item in buildingMaterials)
			{
				item.Value.buffer.SetData (clearedTilePoints, 0, (x + ((height - y - 1) * width)) * clearedTilePoints.Length, clearedTilePoints.Length);
				item.Value.material.SetBuffer ("points", item.Value.buffer);
			}
		}

		DetailMaterial detail = detailMaterials.Find (a => a.x == x && a.y == y);

		if (detail != null)
		{
			foreach (KeyValuePair<int, MaterialType> item in detail.materials) materialTypePool.Enqueue (item.Value);
		}

		if (detailTypes != null && detailTypes.Length > 0)
		{
			if (detail == null)
			{
				detail = new DetailMaterial (x, y);
				detailMaterials.Add (detail);
			}

			detail.materials.Clear ();
			detail.detailsCount = 0;
			for (int i = 0; i < detailTypes.Length; i++) addDetail (detail, detailTypes[i]);
		}
		else
		{
			detailMaterials.Remove (detail);
		}
	}

	public bool InInChunk (int x, int y)
	{
		return x >= baseX && x < baseX + width && y >= baseY && y < baseY + height;
	}

	private void addDetail (DetailMaterial detailMaterial, MapRenderer.TileRendererType detailType)
	{
		if (detailType != null)
		{
			MaterialType materialType;
			if (!detailMaterial.materials.TryGetValue (detailType.materialIndex, out materialType))
			{
				// The material hasn't been added yet so we should do that
				// We need to get the material and texture for it, this should be in the ordinary list of materials
				MaterialType copyMaterial;
				if (tileMaterials.TryGetValue (detailType.materialIndex, out copyMaterial))
				{
					materialType = getMaterialType (copyMaterial.material, copyMaterial.texture);
					detailMaterial.materials.Add (detailType.materialIndex, materialType);
				}
				else
				{
					Debug.LogError ($"MapChunkRenderer.addDetail: No material with index {detailType.materialIndex}");
				}
			}

			setupPointsArray (detailMaterial.x, detailMaterial.y, detailType, whiteColour);
			materialType.buffer.SetData (tilePoints, 0, detailMaterial.detailsCount * tilePoints.Length, tilePoints.Length);
			materialType.material.SetBuffer ("points", materialType.buffer);
			detailMaterial.detailsCount++;
		}
	}

	private void setupPointsArray (int x, int y, MapRenderer.TileRendererType type, Vector4 colour)
	{
		for (int i = 0; i < tilePoints.Length; i++) tilePoints[i].colour = colour;

		float depth = (baseX + x + (0.1f * (baseY + y))) * 0.1f;

		tilePoints[0].vertex = new Vector3 (type.offset.x + baseX + x				, type.offset.y + baseY + y				 , type.offset.z + depth);
		tilePoints[1].vertex = new Vector3 (type.offset.x + baseX + x				, type.offset.y + baseY + y + type.size.y, type.offset.z + depth);
		tilePoints[2].vertex = new Vector3 (type.offset.x + baseX + x + type.size.x , type.offset.y + baseY + y + type.size.y, type.offset.z + depth);
		tilePoints[3].vertex = new Vector3 (type.offset.x + baseX + x + type.size.x , type.offset.y + baseY + y + type.size.y, type.offset.z + depth);
		tilePoints[4].vertex = new Vector3 (type.offset.x + baseX + x + type.size.x , type.offset.y + baseY + y				 , type.offset.z + depth);
		tilePoints[5].vertex = new Vector3 (type.offset.x + baseX + x				, type.offset.y + baseY + y				 , type.offset.z + depth);

		tilePoints[0].uv = type.uv0;
		tilePoints[1].uv = type.uv1;
		tilePoints[2].uv = type.uv2;
		tilePoints[3].uv = type.uv2;
		tilePoints[4].uv = type.uv3;
		tilePoints[5].uv = type.uv0;
	}

	private MaterialType getMaterialType (Material material, Texture2D texture)
	{
		MaterialType type;
		if (materialTypePool.Count > 0)
		{
			type = materialTypePool.Dequeue ();

			if (type.material != null) Object.Destroy (type.material);
			type.material = new Material (material);
			type.material.mainTexture = texture;

			clearMaterialBuffer (type);

			return type;
		}

		material = new Material (material);
		material.mainTexture = texture;

		return new MaterialType (material, texture, pointsCount);
	}

	private void clearMaterialBuffer (MaterialType type)
	{
		for (int i = 0; i < pointsCount; i += clearedTilePoints.Length) type.buffer.SetData (clearedTilePoints, 0, i, clearedTilePoints.Length);
		type.material.SetBuffer ("points", type.buffer);
	}
}
