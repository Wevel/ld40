﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapRenderer : MonoBehaviour
{
	public class TileRendererType
	{
		public readonly uint hashCode;
		public readonly Sprite sprite;
		public readonly int materialIndex;
		public readonly Vector3 offset;
		public readonly Vector2 size;
		public readonly Vector2 uv0;
		public readonly Vector2 uv1;
		public readonly Vector2 uv2;
		public readonly Vector2 uv3;

		public TileRendererType (uint hashCode, Sprite sprite, int materialIndex, Rect spritePackerRect, int spritePackerSizeX, int spritePackerSizeY, Vector3 offset, Vector2 size)
		{
			this.hashCode = hashCode;
			this.sprite = sprite;
			this.materialIndex = materialIndex;
			this.offset = offset;
			this.size = size;
			uv0 = new Vector2 (spritePackerRect.xMin / spritePackerSizeX, spritePackerRect.yMin / spritePackerSizeY);
			uv1 = new Vector2 (spritePackerRect.xMin / spritePackerSizeX, spritePackerRect.yMax / spritePackerSizeY);
			uv2 = new Vector2 (spritePackerRect.xMax / spritePackerSizeX, spritePackerRect.yMax / spritePackerSizeY);
			uv3 = new Vector2 (spritePackerRect.xMax / spritePackerSizeX, spritePackerRect.yMin / spritePackerSizeY);
		}
	}

	private static HashSet<uint> loggedNoTileTypeIDs = new HashSet<uint> ();

	private static void logNoTileType (uint key)
	{
		if (!loggedNoTileTypeIDs.Contains (key))
		{
			Debug.Log ($"MapRenderer.logNoTileType: No tile type with id {key}");
			loggedNoTileTypeIDs.Add (key);
		}
	}

	public Material material;
	public int spritePackerSize = 512;
	public int spritePackerPadding = 4;

	public int chunkSize { get; set; }

	private Map _map;
	public Map map
	{
		get
		{
			return _map;
		}
		set
		{
			if (_map != null) _map.onUpdateTile -= UpdateTile;
			_map = value;
			if (_map != null) _map.onUpdateTile += UpdateTile;
		}
	}

	private List<Texture2D> spriteSheets = new List<Texture2D> ();
	private Dictionary<uint, TileRendererType> tileTypes = new Dictionary<uint, TileRendererType> ();
	private Dictionary<uint, TileRendererType> detailTypes = new Dictionary<uint, TileRendererType> ();
	private Dictionary<uint, TileRendererType> buildingTypes = new Dictionary<uint, TileRendererType> ();

	private Dictionary<int, List<MapChunkRenderer>> chunkRenderers = new Dictionary<int, List<MapChunkRenderer>> ();
	private Queue<MapChunkRenderer> chunkRendererPool = new Queue<MapChunkRenderer> ();

	private SpritePacker currentSpritePacker;

	public Camera mapCamera;

	private void Awake ()
	{
		currentSpritePacker = new SpritePacker (spritePackerSize, spritePackerSize, spritePackerPadding);
		addTexture (currentSpritePacker.GetTexture ());
	}

	private void LateUpdate ()
	{
		if (map != null)
		{
			map.onUpdateTile -= UpdateTile;

			float halfHeight = Camera.main.orthographicSize;
			float halfWidth = Camera.main.aspect * halfHeight;

			Vector2 bottomLeft = (Vector2)Camera.main.transform.position - new Vector2 (halfWidth, halfHeight);
			Vector2 topRight = (Vector2)Camera.main.transform.position + new Vector2 (halfWidth, halfHeight);

			int leftChunk = Mathf.FloorToInt ((bottomLeft.x + 0.5f) / chunkSize);
			int bottomChunk = Mathf.FloorToInt ((bottomLeft.y + 0.5f) / chunkSize);

			int rightChunk = Mathf.FloorToInt ((topRight.x + 0.5f) / chunkSize);
			int topChunk = Mathf.FloorToInt ((topRight.y + 0.5f) / chunkSize);

			//leftChunk = 0;
			//bottomChunk = 0;
			
			//rightChunk = 0;
			//topChunk = 0;

			// Need to remove old renderers if ther are any that shouldn't be shown
			if (chunkRenderers.Count > 0)
			{
				List<int> toRemove = new List<int> ();
				foreach (KeyValuePair<int, List<MapChunkRenderer>> item in chunkRenderers)
				{
					// Reverse loop as we could be removing items
					for (int i = item.Value.Count - 1; i >= 0; i--)
					{
						if (item.Value[i].chunkX < leftChunk || item.Value[i].chunkX > rightChunk ||
							item.Value[i].chunkY < bottomChunk || item.Value[i].chunkY > topChunk)
						{
							chunkRendererPool.Enqueue (item.Value[i]);
							item.Value.RemoveAt (i);
						}
					}

					if (item.Value.Count == 0) toRemove.Add (item.Key);
				}

				for (int i = 0; i < toRemove.Count; i++) chunkRenderers.Remove (toRemove [i]);
			}

			for (int cx = leftChunk; cx <= rightChunk; cx++)
			{
				for (int cy = bottomChunk; cy <= topChunk; cy++)
				{
					addChunk (cx, cy);
				}
			}

			map.onUpdateTile += UpdateTile;
		}
	}

	private void OnRenderObject ()
	{
		if (!enabled) return;
		if (Camera.current != mapCamera) return;

		foreach (KeyValuePair<int, List<MapChunkRenderer>> item in chunkRenderers)
		{
			for (int i = 0; i < item.Value.Count; i++) item.Value[i].RenderChunk ();
		}
	}

	private void OnDestroy ()
	{
		foreach (KeyValuePair<int, List<MapChunkRenderer>> item in chunkRenderers)
		{
			for (int i = 0; i < item.Value.Count; i++) item.Value[i].Dispose ();
		}

		while (chunkRendererPool.Count > 0) chunkRendererPool.Dequeue ().Dispose ();
		chunkRenderers.Clear ();
	}

	public void AddTileType (TileType type)
	{
		if (tileTypes.ContainsKey (type.id))
		{
			Debug.LogWarning ($"MapRenderer.AddTileType: Already a tile type with key {type.id}");
			return;
		}

		Rect spriteSheetUV;
		if (!currentSpritePacker.AddSprite (type.display, out spriteSheetUV))
		{
			// The sprite sheed was full, so make a new one
			spriteSheets[spriteSheets.Count - 1] = currentSpritePacker.GetTexture ();

			currentSpritePacker = new SpritePacker (
				Mathf.Max (spritePackerSize, (int)type.display.rect.width + spritePackerPadding + spritePackerPadding),
				Mathf.Max (spritePackerSize, (int)type.display.rect.height + spritePackerPadding + spritePackerPadding),
				spritePackerPadding);
			addTexture (currentSpritePacker.GetTexture ());

			if (!currentSpritePacker.AddSprite (type.display, out spriteSheetUV))
				Debug.LogError ("MapRenderer.AddTileType: Failed to add sprite to sprite sheet, even though it was made to be the correct size for the new sprite");
		}

		spriteSheets[spriteSheets.Count - 1] = currentSpritePacker.GetTexture ();

		Rect baseSpriteRect = type.display.rect;

		tileTypes.Add (type.id, new TileRendererType (
			type.id,
			type.display,
			spriteSheets.Count - 1,
			spriteSheetUV,
			currentSpritePacker.width,
			currentSpritePacker.height,
			type.displayOffset - new Vector3 (type.display.pivot.x / baseSpriteRect.width, type.display.pivot.y / baseSpriteRect.height),
			new Vector2 (baseSpriteRect.width / type.display.pixelsPerUnit, baseSpriteRect.height / type.display.pixelsPerUnit)));
	}

	public void AddDetailType (DetailType type)
	{
		if (detailTypes.ContainsKey (type.id))
		{
			Debug.LogWarning ($"MapRenderer.AddDetailType: Already a detail type with key {type.id}");
			return;
		}

		Rect spriteSheetUV;
		if (!currentSpritePacker.AddSprite (type.display, out spriteSheetUV))
		{
			// The sprite sheed was full, so make a new one
			spriteSheets[spriteSheets.Count - 1] = currentSpritePacker.GetTexture ();

			currentSpritePacker = new SpritePacker (
				Mathf.Max (spritePackerSize, (int)type.display.rect.width + spritePackerPadding + spritePackerPadding),
				Mathf.Max (spritePackerSize, (int)type.display.rect.height + spritePackerPadding + spritePackerPadding),
				spritePackerPadding);
			addTexture (currentSpritePacker.GetTexture ());

			if (!currentSpritePacker.AddSprite (type.display, out spriteSheetUV))
				Debug.LogError ("MapRenderer.AddDetailType: Failed to add sprite to sprite sheet, even though it was made to be the correct size for the new sprite");
		}

		spriteSheets[spriteSheets.Count - 1] = currentSpritePacker.GetTexture ();

		Rect baseSpriteRect = type.display.rect;

		detailTypes.Add (type.id, new TileRendererType (
			type.id,
			type.display,
			spriteSheets.Count - 1,
			spriteSheetUV,
			currentSpritePacker.width,
			currentSpritePacker.height,
			type.displayOffset - new Vector3 (type.display.pivot.x / baseSpriteRect.width, type.display.pivot.y / baseSpriteRect.height),
			new Vector2 (baseSpriteRect.width / type.display.pixelsPerUnit, baseSpriteRect.height / type.display.pixelsPerUnit)));
	}

	public void AddBuildingType (BuildingType type)
	{
		if (buildingTypes.ContainsKey (type.id))
		{
			Debug.LogWarning ($"MapRenderer.AddBuildingType: Already a building type with key {type.id}");
			return;
		}

		Rect spriteSheetUV;
		if (!currentSpritePacker.AddSprite (type.display, out spriteSheetUV))
		{
			// The sprite sheed was full, so make a new one
			spriteSheets[spriteSheets.Count - 1] = currentSpritePacker.GetTexture ();

			currentSpritePacker = new SpritePacker (
				Mathf.Max (spritePackerSize, (int)type.display.rect.width + spritePackerPadding + spritePackerPadding),
				Mathf.Max (spritePackerSize, (int)type.display.rect.height + spritePackerPadding + spritePackerPadding),
				spritePackerPadding);
			addTexture (currentSpritePacker.GetTexture ());

			if (!currentSpritePacker.AddSprite (type.display, out spriteSheetUV))
				Debug.LogError ("MapRenderer.AddTileType: Failed to add sprite to sprite sheet, even though it was made to be the correct size for the new sprite");
		}

		spriteSheets[spriteSheets.Count - 1] = currentSpritePacker.GetTexture ();

		Rect baseSpriteRect = type.display.rect;

		buildingTypes.Add (type.id, new TileRendererType (
			type.id,
			type.display,
			spriteSheets.Count - 1,
			spriteSheetUV,
			currentSpritePacker.width,
			currentSpritePacker.height,
			type.displayOffset - new Vector3 (type.display.pivot.x / baseSpriteRect.width, type.display.pivot.y / baseSpriteRect.height),
			new Vector2 (baseSpriteRect.width / type.display.pixelsPerUnit, baseSpriteRect.height / type.display.pixelsPerUnit)));
	}

	public void UpdateTile (Tile tile)
	{
		UpdateTile (tile.x, tile.y);
	}

	public void UpdateTile (int x, int y)
	{
		updateSingleTile (x - 1, y + 1);
		updateSingleTile (x - 1, y);
		updateSingleTile (x - 1, y - 1);

		updateSingleTile (x, y + 1);
		updateSingleTile (x, y);
		updateSingleTile (x, y - 1);

		updateSingleTile (x + 1, y + 1);
		updateSingleTile (x + 1, y);
		updateSingleTile (x + 1, y - 1);
	}

	public void Clear ()
	{
		foreach (KeyValuePair<int, List<MapChunkRenderer>> item in chunkRenderers)
		{
			for (int i = 0; i < item.Value.Count; i++) chunkRendererPool.Enqueue (item.Value[i]);
		}
		chunkRenderers.Clear ();

		tileTypes.Clear ();
		detailTypes.Clear ();
		buildingTypes.Clear ();
		spriteSheets.Clear ();

		currentSpritePacker = new SpritePacker (spritePackerSize, spritePackerSize, spritePackerPadding);
		addTexture (currentSpritePacker.GetTexture ());
	}

	private void addChunk (int cx, int cy)
	{
		// Make sure we dont already have the chunk

		List<MapChunkRenderer> columnChunkRenderers;
		if (!chunkRenderers.TryGetValue (cx, out columnChunkRenderers))
		{
			columnChunkRenderers = new List<MapChunkRenderer> ();
			chunkRenderers.Add (cx, columnChunkRenderers);
		}

		if (columnChunkRenderers.Find (x => x.chunkX == cx && x.chunkY == cy) == null)
		{
			bool inserted = false;
			MapChunkRenderer cr = getChunkRenderer ();
			//for (int i = 0; i < columnChunkRenderers.Count; i++)
			//{
			//	if (columnChunkRenderers[i].chunkY <= cr.chunkY)
			//	{
			//		columnChunkRenderers.Insert (i, cr);
			//		inserted = true;
			//		break;
			//	}
			//}
			if (!inserted) columnChunkRenderers.Add (cr);
			//columnChunkRenderers.Sort ((a, b) => { return b.chunkY.CompareTo (a.chunkY); });
			cr.Setup (cx, cy);

			for (int i = 0; i < spriteSheets.Count; i++) cr.AddMaterial (i, material, spriteSheets[i]);

			Tile tile;
			for (int x = 0; x < chunkSize; x++)
			{
				for (int y = 0; y < chunkSize; y++)
				{
					tile = map.GetTile (cr.baseX + x, cr.baseY + y);
					cr.SetTile (cr.baseX + x, cr.baseY + y, getRendererType (tile), getRendererDetailTypes (tile), getRendererBuildingType (tile), tile.building != null && tile.building.isBlueprint);
				}
			}
		}
	}

	private TileRendererType getRendererType (Tile tile)
	{
		TileRendererType type = null;
		if (tile.type != null)
		{
			if (!tileTypes.TryGetValue (tile.type.id, out type)) logNoTileType (tile.type.id);
		}
		return type;
	}

	private TileRendererType[] getRendererDetailTypes (Tile tile)
	{
		List<uint> detailTypes = tile.GetDetailTypes ();
		TileRendererType[] types = new TileRendererType[detailTypes.Count];
		for (int i = 0; i < detailTypes.Count; i++) types[i] = getRendererDetailType (detailTypes[i]);
		return types;
	}

	private TileRendererType getRendererDetailType (uint id)
	{
		TileRendererType type = null;
		if (!detailTypes.TryGetValue (id, out type)) logNoTileType (id);
		return type;
	}

	private TileRendererType getRendererBuildingType (Tile tile)
	{
		TileRendererType type = null;
		if (tile.building != null)
		{
			if (!buildingTypes.TryGetValue (tile.building.type.id, out type)) logNoTileType (tile.type.id);
		}
		return type;
	}

	private MapChunkRenderer getChunkRenderer ()
	{
		if (chunkRendererPool.Count > 0) return chunkRendererPool.Dequeue ();
		return new MapChunkRenderer (chunkSize);
	}

	private void updateSingleTile (int x, int y)
	{
		int chunkX = Mathf.FloorToInt ((float)x / chunkSize);
		int chunkY = Mathf.FloorToInt ((float)y / chunkSize);

		List<MapChunkRenderer> columnChunkRenderers;
		if (chunkRenderers.TryGetValue (chunkX, out columnChunkRenderers))
		{
			MapChunkRenderer chunk = columnChunkRenderers.Find (a => a.chunkX == chunkX && a.chunkY == chunkY);

			// We don't care if the chunk isn't shown, as there is nothing to update on the screen at the moment
			if (chunk != null)
			{
				Tile tile = map.GetTile (x, y);
				chunk.SetTile (x, y, getRendererType (tile), getRendererDetailTypes (tile), getRendererBuildingType (tile), tile.building != null && tile.building.isBlueprint);
			}
		}
	}

	private void addTexture (Texture2D texture)
	{
		texture.filterMode = FilterMode.Point;
		texture.wrapMode = TextureWrapMode.Clamp;
		spriteSheets.Add (texture);
	}
}
