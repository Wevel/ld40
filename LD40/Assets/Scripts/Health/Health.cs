﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Health
{
	public readonly float maxHealth;
	public readonly float healRate;
	public readonly float healDelay;
	public float currentHealth { get; private set; }

	private float healTimer;
	public Vector2 position { get; protected set; }

	public readonly Vector2 size;
	public readonly Vector2 offset;

	public bool IsAlive
	{
		get
		{
			return currentHealth > 0;
		}
	}

	protected Health (float maxHealth, float healRate, float healDelay, Vector2 size, Vector2 offset)
	{
		this.maxHealth = maxHealth;
		this.healRate = healRate;
		this.healDelay = healDelay;
		this.size = size;
		this.offset = offset;

		currentHealth = maxHealth;

		healTimer = healDelay;
	}

	public virtual void Update (float deltaTime)
	{
		if (currentHealth < maxHealth)
		{
			if (healTimer > 0)
			{
				healTimer -= deltaTime;
			}
			else
			{
				currentHealth += deltaTime * healRate;
				if (currentHealth > maxHealth) currentHealth = maxHealth;
			}
		}
	}

	public void DoDamage (float damage)
	{
		currentHealth -= damage;
		healTimer = healDelay;

		if (!IsAlive) Die ();
	}

	public bool DoesOverlap (Health other)
	{
		Rect thisArea = new Rect (this.position + this.offset - (this.size / 2), this.size);
		Rect otherArea = new Rect (other.position + other.offset - (other.size / 2), other.size);

		return thisArea.Overlaps (otherArea);
	}

	protected abstract void Die ();
}
