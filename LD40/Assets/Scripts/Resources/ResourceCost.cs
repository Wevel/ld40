﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourceCost 
{
	public string key;
	public uint count;
	public bool giveCorruption;
}
