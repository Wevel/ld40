﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager 
{
	public const string corruptionResourceKey = "corruption";
	const int ironProductionAmount = 5;
	const int ironOreCost = 5;
	const int coalCost = 5;

	public readonly Map map;
	public readonly ResourceType corruptionResource;

	private Dictionary<ResourceType, uint> resourceCounts = new Dictionary<ResourceType, uint> ();

	private List<ResourceType> resourceTypes;

	private float accurateCorruptionCount = 0;

	public float ExactCorruption
	{
		get
		{
			return accurateCorruptionCount + GetResourceCount (corruptionResource) + 50;
		}
	}

	public IEnumerable<ResourceType> ResourceTypes
	{
		get
		{
			for (int i = 0; i < resourceTypes.Count; i++) yield return resourceTypes[i];
		}
	}

	public ResourceManager (Map map, List<ResourceType> resourceTypes)
	{
		this.map = map;
		this.resourceTypes = new List<ResourceType> (resourceTypes);
		for (int i = 0; i < resourceTypes.Count; i++)
		{
			resourceCounts.Add (resourceTypes[i], resourceTypes[i].startingCount);
			if (resourceTypes[i].key == corruptionResourceKey) corruptionResource = resourceTypes[i];
		}
	}

	public ResourceType GetResourceType (string key)
	{
		return resourceTypes.Find (x => x.key == key);
	}

	public uint GetResourceCount (string typeKey)
	{
		ResourceType type = GetResourceType (typeKey);
		if (type == null) return 0;

		uint count = 0;
		resourceCounts.TryGetValue (type, out count);
		return count;
	}

	public uint GetResourceCount (ResourceType type)
	{
		uint count = 0;
		resourceCounts.TryGetValue (type, out count);
		return count;
	}

	public bool TryUseResources (ResourceType type, uint amount)
	{
		uint count = 0;
		if (resourceCounts.TryGetValue (type, out count))
		{
			if (count >= amount)
			{
				resourceCounts[type] = count - amount;
				return true;
			}
		}
		else
		{
			if (amount == 0) return true;
		}

		return false;
	}

	public bool CanUseResources (ResourceType type, uint amount)
	{
		uint count = 0;
		if (resourceCounts.TryGetValue (type, out count))
		{
			if (count >= amount)
			{
				return true;
			}
		}
		else
		{
			if (amount == 0) return true;
		}

		return false;
	}

	public void AddResources (ResourceType type, uint amount)
	{
		uint count = 0;
		if (resourceCounts.TryGetValue (type, out count))
		{
			resourceCounts[type] = count + amount;
		}
		else
		{
			resourceCounts[type] = amount;
		}
	}

	public void AddResourcesFromMining (TileType type)
	{
		if (type.mineFinalType != "")
		{
			ResourceType resource;
			for (int i = 0; i < type.mineProducts.Count; i++)
			{
				resource = GetResourceType (type.mineProducts[i].key);
				if (resource != null)
				{
					AddResources (resource, type.mineProducts[i].count);

					if (type.mineProducts[i].giveCorruption) addCorruption (resource, type.mineProducts[i].count);
				}
				else
				{
					Debug.LogWarning ($"ResourceManager.AddResourcesFromMining: No resource type with key {type.mineProducts[i].key}");
				}
			}
		}
	}

	public void AddResourcesFromDeconstruct (BuildingType type)
	{
		ResourceType resource;
		for (int i = 0; i < type.deconstructReturn.Count; i++)
		{
			resource = GetResourceType (type.deconstructReturn[i].key);
			if (resource != null)
			{
				uint amountCreated = (uint)Mathf.RoundToInt (type.deconstructReturn[i].count * (1f + map.modifierManager.GetModifier ($"{type.key}_{resource.key}_return")));
				AddResources (resource, amountCreated);
				if (type.deconstructReturn[i].giveCorruption) addCorruption (resource, amountCreated);
			}
			else
			{
				Debug.LogWarning ($"ResourceManager.AddResourcesFromDeconstruct: No resource type with key {type.deconstructReturn[i].key}");
			}
		}
	}

	public void SmithIron ()
	{
		ResourceType ironResource = GetResourceType ("iron");
		ResourceType ironOreResource = GetResourceType ("ironOre");
		ResourceType coalResource = GetResourceType ("coal");

		if (!CanUseResources (ironOreResource, ironOreCost) || !CanUseResources (coalResource, coalCost)) return;
		Debug.Assert (TryUseResources (ironOreResource, ironOreCost) && TryUseResources (coalResource, coalCost),
			"ResourceManager.SmithIron: Had the resources need but now cant smith iron");

		uint amountCreated = (uint)Mathf.RoundToInt (ironProductionAmount * (1f + map.modifierManager.GetModifier ("ironProduction")));
		AddResources (ironResource, amountCreated);
		addCorruption (ironResource, amountCreated);
	}

	public bool CanSmithIron ()
	{
		ResourceType ironOreResource = GetResourceType ("ironOre");
		ResourceType coalResource = GetResourceType ("coal");

		return CanUseResources (ironOreResource, ironOreCost) && CanUseResources (coalResource, coalCost);
	}

	public bool CanUseBuildingResources (BuildingType type)
	{
		ResourceType resourceType;
		for (int i = 0; i < type.buildCost.Count; i++)
		{
			resourceType = GetResourceType (type.buildCost[i].key);
			if (resourceType == null || !CanUseResources (resourceType, type.buildCost[i].count)) return false;
		}

		return true;
	}

	public bool TryUseResearchResources (Research research)
	{
		ResourceType goldType = GetResourceType ("gold");
		ResourceType gemsType = GetResourceType ("gems");

		if (CanUseResources (goldType, research.type.goldNeeded) && CanUseResources (gemsType, research.type.gemsNeeded))
		{
			return TryUseResources (goldType, research.type.goldNeeded) && TryUseResources (gemsType, research.type.gemsNeeded);
		}

		return false;
	}

	public bool TryUseBuildingResources (BuildingType type)
	{
		ResourceType resourceType;
		for (int i = 0; i < type.buildCost.Count; i++)
		{
			resourceType = GetResourceType (type.buildCost[i].key);
			if (resourceType == null || !CanUseResources (resourceType, type.buildCost[i].count)) return false;
		}

		for (int i = 0; i < type.buildCost.Count; i++)
		{
			resourceType = GetResourceType (type.buildCost[i].key);
			if (resourceType == null || !TryUseResources (resourceType, type.buildCost[i].count))
			{
				Debug.LogError ("ResourceManager.TryUseBuildingResources: Was allowed to start using resources but then could't actually use them");
				return false;
			}
		}

		return true;
	}

	private void addCorruption (ResourceType type, uint count)
	{
		float correctCount = count * type.gatherCorruptionChance * (1.0f - map.modifierManager.GetModifier ("curruptionReduction"));
		accurateCorruptionCount += correctCount;

		uint finalCount = (uint)Mathf.FloorToInt (accurateCorruptionCount);
		accurateCorruptionCount -= finalCount;

		AddResources (corruptionResource, finalCount);
	}
}
