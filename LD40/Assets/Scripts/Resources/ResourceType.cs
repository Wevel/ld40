﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourceType 
{
	public string key;
	public string name;
	public Sprite icon;
	public float gatherCorruptionChance;
	public uint startingCount;
}
