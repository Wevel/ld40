﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worker : Entity 
{
	public Building assignedBuilding { get; private set; }

	private bool hasBeenToAssignedBuilding = true;

	public Worker (Map map, EntityType type, Tile initialTile) : base (map, type, initialTile)
	{

	}

	public override void Update (float deltaTime)
	{
		base.Update (deltaTime);

		if (assignedBuilding != null)
		{
			if (currentTile == assignedBuilding.tile) hasBeenToAssignedBuilding = true;
		}
	}

	public override Task[] GetNewTasks ()
	{
		if (!hasBeenToAssignedBuilding)
		{
			return new Task[] { new MoveTask (map.GetTile(0, 0)), new MoveTask (assignedBuilding.tile) };
		}
		else if (currentJob != null)
		{
			return currentJob.GetInitialTasks (this);
		}
		else
		{
			if (assignedBuilding == null) return new Task[] { new MoveTask (GetRandomMoveTarget ()) };

			if (assignedBuilding.type.key == "workerTools")
			{
				Job newJob;
				if (map.jobManger.TryFindJob (this, out newJob))
				{
					currentJob = newJob;
					return currentJob.GetInitialTasks (this);
				}
				else
				{
					if (Node.Distance (currentTile, assignedBuilding.tile) > 5)
					{
						currentJob = new MoveJob (assignedBuilding.tile);
						currentJob.TakeJob (this);
						return currentJob.GetInitialTasks (this);
					}
					else
					{
						return new Task[] { new MoveTask (GetRandomMoveTarget ()) };
					}
				}
			}
			else
			{
				Job newJob;
				if (tryGetJob (out newJob))
				{
					currentJob = newJob;
					return currentJob.GetInitialTasks (this);
				}
				else
				{
					return new Task[] { new MoveTask (GetRandomMoveTarget ()) };
				}
			}
		}
	}

	public void AssignBuilding (Building building)
	{
		this.assignedBuilding = building;
		hasBeenToAssignedBuilding = false;
	}

	public void UnAssignBuilding ()
	{
		this.assignedBuilding = null;
		hasBeenToAssignedBuilding = true;
	}

	private bool tryGetJob (out Job job)
	{
		if (assignedBuilding.type.key == "researchBench") job = createJobForResearch ();
		else if (assignedBuilding.type.key == "blacksmith") job = createJobForBlacksmith ();
		else if (assignedBuilding.type.key == "armoury") job = createJobForArmoury ();
		else job = null;

		if (job != null) job.TakeJob (this);
		return job != null;
	}

	private Job createJobForResearch ()
	{
		if (map.researchManager.CanResearch) return new ResearchJob (assignedBuilding.tile);
		if (Node.Distance (currentTile, assignedBuilding.tile) > 5) return new MoveJob (assignedBuilding.tile);
		else return null;
	}

	private Job createJobForBlacksmith ()
	{
		if (map.resourceManager.CanSmithIron ()) return WorkJob.ProduceIronJob (assignedBuilding.tile, 4);
		if (Node.Distance (currentTile, assignedBuilding.tile) > 5) return new MoveJob (assignedBuilding.tile);
		else return null;
	}

	private Job createJobForArmoury ()
	{
		// Basically go to the furthest wall and stand behind it

		Tile tile;
		for (int x = -40; x < 4; x++)
		{
			tile = map.GetTile (x, 0);

			if (tile.building != null && !tile.building.isBlueprint)
			{
				if (tile.building.type.key.Contains ("Wall")) return new GuardJob (tile);
			}
		}

		if (Node.Distance (currentTile, new Node (0, 0)) > 5) return new MoveJob (map.GetTile (0, 0));
		else return null;
	}
}
