﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterruptibleTaskQueue 
{
	public readonly Entity entity;

	private Queue<Task> taskQueue = new Queue<Task> ();
	public Task currentTask { get; private set; }

	public bool interrupted { get; private set; }

	public InterruptibleTaskQueue (Entity entity)
	{
		this.entity = entity;

		NextTask ();
	}

	public void UpdateTasks (float deltaTime)
	{
		interrupted = false;

		if (currentTask != null) currentTask.Update (this, deltaTime);
		else NextTask ();
	}

	public void NextTask ()
	{
		if (taskQueue.Count > 0)
		{
			currentTask = taskQueue.Dequeue ();
			currentTask.Start (this);
		}
		else
		{
			Task[] newTasks = entity.GetNewTasks ();
			if (newTasks != null && newTasks.Length > 0)
			{
				taskQueue = new Queue<Task> (newTasks);
				NextTask ();
			}
		}
	}

	public void InteruptTask (params Task[] newTasks)
	{
		interrupted = true;

		Queue<Task> newTaskQueue = new Queue<Task> (newTasks);

		if (currentTask != null)
		{
			currentTask.Pause (this);
			newTaskQueue.Enqueue (currentTask);
		}

		while (taskQueue.Count > 0) newTaskQueue.Enqueue (taskQueue.Dequeue ());

		taskQueue = newTaskQueue;

		NextTask ();
	}

	public void FailCurrentTask ()
	{
		currentTask = null;
		taskQueue.Clear ();
		interrupted = true;

		entity.FailCurrentJob ();
	}

	public void ClearQueue ()
	{
		currentTask = null;
		taskQueue.Clear ();
		interrupted = true;
	}
}
