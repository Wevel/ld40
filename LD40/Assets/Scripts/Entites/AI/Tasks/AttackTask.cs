﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTask : Task
{
	public readonly Entity targetEntity;

	public AttackTask (Entity targetEntity, TaskSuccessful onSuccess = null) : base (onSuccess)
	{
		this.targetEntity = targetEntity;
	}

	protected override void Onstart (InterruptibleTaskQueue manager)
	{
		// If the type is already correct, then we don't even need to path to it
		if (!targetEntity.IsAlive) return;

		Tile closest = getClosestNeighbour (targetEntity);
		if (closest == null) manager.FailCurrentTask ();
		if (!manager.entity.currentTile.IsNeighbour (targetEntity.currentTile)) manager.InteruptTask (new MoveTask (closest, 5));
	}

	protected override bool OnUpdate (InterruptibleTaskQueue manager, float deltaTime)
	{
		if (!targetEntity.IsAlive) return true;

		Tile closest = getClosestNeighbour (targetEntity);
		if (closest == null) manager.FailCurrentTask ();
		if (!manager.entity.currentTile.IsNeighbour (targetEntity.currentTile))
		{
			manager.InteruptTask (new MoveTask (closest, 5));
			return false;
		}

		//ToDo:
		return true;
		//if (remainingWorkTime > 0) remainingWorkTime -= deltaTime;
		//return remainingWorkTime <= 0;
	}

	private Tile getClosestNeighbour (Entity entity)
	{
		Tile best = null;
		float bestDistance = float.MaxValue;
		float tmpDistance;

		foreach (Tile item in entity.map.GetWalkableNeighbours (entity.currentTile))
		{
			tmpDistance = Node.SqrDistance (entity.currentTile, entity.currentTile);
			if (best == null || tmpDistance < bestDistance)
			{
				best = item;
				bestDistance = tmpDistance;
			}
		}

		return best;
	}
}