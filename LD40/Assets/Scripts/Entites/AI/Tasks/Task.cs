﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Task
{
	private bool started = false;
	private TaskSuccessful onSuccess;
	public bool isValid { get; protected set; }

	public Task (TaskSuccessful onSuccess)
	{
		this.onSuccess = onSuccess;
		isValid = true;
	}

	public void Start (InterruptibleTaskQueue manager)
	{
		if (!started)
		{
			started = true;
			Onstart (manager);
		}
	}

	public void Pause (InterruptibleTaskQueue manager)
	{
		if (started)
		{
			started = false;
			OnPause (manager);
		}
	}

	public void Update (InterruptibleTaskQueue manager, float deltaTime)
	{
		if (!started) Start (manager);

		if (OnUpdate (manager, deltaTime))
		{
			started = false;

			OnFinish(manager);
			onSuccess?.Invoke ();
			if (!manager.interrupted) manager.NextTask ();
		}
	}

	protected virtual void Onstart (InterruptibleTaskQueue manager) { }
	protected virtual void OnPause (InterruptibleTaskQueue manager) { }
	protected virtual void OnFinish (InterruptibleTaskQueue manager) { }
	protected virtual bool OnUpdate (InterruptibleTaskQueue manager, float deltaTime)
	{
		return true;
	}
}
