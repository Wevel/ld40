﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTask : Task
{
	public readonly Tile target;
	public readonly int maxMoves;

	private Path currentPath = null;

	public MoveTask (Tile target, TaskSuccessful onSuccess = null) : base (onSuccess)
	{
		this.target = target;
		this.maxMoves = int.MaxValue;
		if (target == null) isValid = false;
	}

	public MoveTask (Tile target, int maxMoves, TaskSuccessful onSuccess = null) : base (onSuccess)
	{
		this.target = target;
		this.maxMoves = maxMoves;
		if (target == null) isValid = false;
	}

	protected override void Onstart (InterruptibleTaskQueue manager)
	{
		if (target == null) return;
		if (manager.entity.currentTile == target) return;
		if (currentPath != null && currentPath.Length > 0 && !target.map.IsWalkable (currentPath.FirstTile)) return;

		if (currentPath == null || currentPath.Length == 0 || manager.entity.currentTile != currentPath.FirstTile)
		{
			currentPath = Pathfinding.GetPath (manager.entity.map, manager.entity, target);
			if (currentPath == null || currentPath.Length == 0)
			{
				// ToDo: Fail the task

				currentPath = null;
				Debug.LogWarning ($"MoveTask.OnStart: No path from {manager.entity.currentTile} to {target}");
			}
			else
			{
				currentPath.RemoveAfter (maxMoves);
			}
		}
	}

	protected override bool OnUpdate (InterruptibleTaskQueue manager, float deltaTime)
	{
		if (target == null) return true;
		if (manager.entity.currentTile == target && Node.SqrDistance (manager.entity.position, target) < 0.0001f) return true;
		if (currentPath != null && currentPath.Length > 0 && !target.map.IsWalkable (currentPath.FirstTile)) return true;

		if (currentPath == null || currentPath.Length == 0 || !manager.entity.map.IsWalkable (currentPath.FirstTile))
		{
			Onstart (manager);
		}

		if (currentPath != null)
		{
			Tile nextTile = currentPath.FirstTile;

			float distance = Node.Distance (manager.entity.position, nextTile);

			if (distance < 0.01f)
			{
				manager.entity.AttemptSetPosition (new Vector2 (nextTile.x, nextTile.y));
				currentPath.RemoveFirstTile ();

				return currentPath.Length == 0;
			}
			else
			{
				manager.entity.AttemptSetPosition (Vector2.Lerp (manager.entity.position, new Vector2 (nextTile.x, nextTile.y), manager.entity.type.speed * deltaTime / distance));
				return false;
			}
		}

		return false;
	}
}
