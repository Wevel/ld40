﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForEnemyTask : Task 
{
	public readonly Tile targetTile;

	public WaitForEnemyTask (Tile targetTile, TaskSuccessful onSuccess = null) : base (onSuccess)
	{
		this.targetTile = targetTile;
	}

	protected override void Onstart (InterruptibleTaskQueue manager)
	{
		if (manager.entity.currentTile != targetTile) manager.InteruptTask (new MoveTask (targetTile));
	}

	protected override bool OnUpdate (InterruptibleTaskQueue manager, float deltaTime)
	{
		if (manager.entity.currentTile != targetTile) manager.InteruptTask (new MoveTask (targetTile));

		if (manager.entity.currentTile.building == null ||
			manager.entity.currentTile.building.isBlueprint ||
			manager.entity.currentTile.building.type.key.Contains ("Wall")) return true;
		// Currently this task can't be completed
		return false;
	}
}
