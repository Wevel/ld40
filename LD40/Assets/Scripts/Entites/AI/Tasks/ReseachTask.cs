﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReseachTask : Task 
{
	public readonly Tile targetTile;

	public ReseachTask (Tile targetTile, TaskSuccessful onSuccess = null) : base (onSuccess)
	{
		this.targetTile = targetTile;
	}

	protected override void Onstart (InterruptibleTaskQueue manager)
	{
		if (manager.entity.currentTile != targetTile) manager.InteruptTask (new MoveTask (targetTile));
	}

	protected override bool OnUpdate (InterruptibleTaskQueue manager, float deltaTime)
	{
		if (manager.entity.currentTile != targetTile) manager.InteruptTask (new MoveTask (targetTile));

		if (manager.entity.currentTile.building == null ||
			manager.entity.currentTile.building.isBlueprint ||
			manager.entity.currentTile.building.type.key != "researchBench") return true;

		targetTile.map.researchManager.DoResearchProgress (deltaTime);

		return !targetTile.map.researchManager.CanResearch;
	}
}
