﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkTask : Task 
{
	public readonly Tile targetTile;
	public readonly TaskPerformed isTaskPerformed;
	public readonly bool standOnTile;

	private float remainingWorkTime;

	public WorkTask (Tile targetTile, float totalWorkTime, bool standOnTile, TaskPerformed isTaskPerformed, TaskSuccessful onSuccess = null) : base (onSuccess)
	{
		this.targetTile = targetTile;
		this.isTaskPerformed = isTaskPerformed;
		this.remainingWorkTime = totalWorkTime;
		this.standOnTile = standOnTile;
	}

	protected override void Onstart (InterruptibleTaskQueue manager)
	{
		// If the type is already correct, then we don't even need to path to it
		if (isTaskPerformed?.Invoke () ?? false) return;

		if (standOnTile)
		{
			if (manager.entity.currentTile != targetTile) manager.InteruptTask (new MoveTask (targetTile));
		}
		else
		{
			Tile closest = getClosestNeighbour (manager.entity);
			if (closest == null) manager.FailCurrentTask ();
			if (!manager.entity.currentTile.IsNeighbour (targetTile)) manager.InteruptTask (new MoveTask (closest));
		}
	}

	protected override bool OnUpdate (InterruptibleTaskQueue manager, float deltaTime)
	{
		if (isTaskPerformed?.Invoke () ?? false) return true;


		if (standOnTile)
		{
			if (manager.entity.currentTile != targetTile)
			{
				manager.InteruptTask (new MoveTask (targetTile));
				return false;
			}
		}
		else
		{
			Tile closest = getClosestNeighbour (manager.entity);
			if (closest == null) manager.FailCurrentTask ();
			if (!manager.entity.currentTile.IsNeighbour (targetTile))
			{
				manager.InteruptTask (new MoveTask (closest));
				return false;
			}
		}

		if (remainingWorkTime > 0) remainingWorkTime -= deltaTime;
		return remainingWorkTime <= 0;
	}

	private Tile getClosestNeighbour (Entity entity)
	{
		Tile best = null;
		float bestDistance = float.MaxValue;
		float tmpDistance;

		foreach (Tile item in entity.map.GetWalkableNeighbours (targetTile))
		{
			tmpDistance = Node.SqrDistance (entity.currentTile, targetTile);
			if (best == null || tmpDistance < bestDistance)
			{
				best = item;
				bestDistance = tmpDistance;
			}
		}

		return best;

	}
}
