﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Job 
{
	public readonly Tile targetTile;

	public bool isValid { get; protected set; }

	public Entity assignedWorker { get; private set; }

	public Job (Tile targetTile)
	{
		this.targetTile = targetTile;
		isValid = true;
	}

	public void TakeJob (Entity entity)
	{
		if (assignedWorker != null && assignedWorker != entity)
		{
			Debug.LogWarning ("Job.TakeJob: Already an entity assigned to this job");
			return;
		}

		assignedWorker = entity;
	}

	public void LeaveJob (Entity enity)
	{
		if (assignedWorker == enity) assignedWorker = null;
		else Debug.LogWarning ("Job.LeaveJob: Worker isn't doing the job");
	}

	public static bool operator == (Job a, Job b)
	{
		if (ReferenceEquals (a, null)) return ReferenceEquals (b, null);
		if (ReferenceEquals (b, null)) return false;
		return a.IsSameJob (b);
	}

	public static bool operator != (Job a, Job b)
	{
		return !(a == b);
	}

	public override int GetHashCode ()
	{
		return base.GetHashCode ();
	}

	public override string ToString ()
	{
		return $"Job{targetTile}";
	}

	public override bool Equals (object obj)
	{
		return base.Equals (obj);
	}

	public abstract bool IsCurrentlyValid (Map map);
	public abstract Task[] GetInitialTasks (Entity entity);
	public abstract float GetWeight (Entity entity);
	public abstract bool IsSameJob (Job other);
}
