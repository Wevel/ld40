﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResearchJob : Job
{
	public ResearchJob (Tile targetTile) : base (targetTile) { }

	public override Task[] GetInitialTasks (Entity entity)
	{
		return new Task[] { new ReseachTask ( targetTile) };
	}

	public override float GetWeight (Entity entity)
	{
		return -Node.SqrDistance (entity.currentTile, targetTile);
	}

	public override bool IsSameJob (Job other)
	{
		return false;
	}

	public override bool IsCurrentlyValid (Map map)
	{
		return map.IsWalkable (targetTile) && map.researchManager.CanResearch;
	}
}
