﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkJob : Job 
{
	public static Job ProduceIronJob (Tile targetTile, float craftTime)
	{
		if (targetTile.building == null || targetTile.building.isBlueprint || targetTile.building.type.key != "blacksmith") return null;

		return new WorkJob (
			targetTile,
			$"smithJob{targetTile}",
			craftTime,
			true,
			() =>
			{
				return targetTile.building == null || targetTile.building.isBlueprint || targetTile.building.type.key != "blacksmith";
			},
			() =>
			{
				targetTile.map.resourceManager.SmithIron ();
			},
			() =>
			{
				return targetTile.map.resourceManager.CanSmithIron ();
			});
	}

	public static Job MineJob (Tile targetTile, float miningTime)
	{
		if (targetTile.type.mineFinalType == "") return null;

		TileType finalTileType = targetTile.map.GetTileType (targetTile.type.mineFinalType);

		return new WorkJob (
			targetTile,
			$"mineJob{targetTile}", 
			miningTime,
			false,
			() => 
			{
				return targetTile.type == finalTileType;
			}, 
			() =>
			{
				targetTile.map.resourceManager.AddResourcesFromMining (targetTile.type);
				targetTile.SetTileType (finalTileType);
			},
			() =>
			{
				return true;
			});
	}

	public static Job DeconstructJob (Tile targetTile, float miningTime)
	{
		if (targetTile.building ==  null) return null;

		return new WorkJob (
			targetTile,
			$"deconstructJob{targetTile}",
			miningTime,
			targetTile.map.IsWalkable(targetTile),
			() =>
			{
				return targetTile.building == null;
			},
			() =>
			{
				targetTile.map.resourceManager.AddResourcesFromDeconstruct (targetTile.building.type);
				targetTile.RemoveBuilding ();
			},
			() => {
				return true;
			});
	}

	public static Job BuildJob (Tile targetTile, BuildingType type, float miningTime)
	{
		if (!targetTile.map.CanBuild (type, targetTile, false)) return null;
		if (!targetTile.map.TryBuild (type, targetTile, true)) return null;

		return new WorkJob (
			targetTile,
			$"buildJob{targetTile}",
			miningTime,
			type.isWalkable,
			() =>
			{
				return (targetTile.building != null && targetTile.building.type == type && !targetTile.building.isBlueprint) || !targetTile.map.CanBuild (type, targetTile, false);
			},
			() =>
			{
				if (targetTile.map.TryBuild (type, targetTile, false))
				{

				}
			},
			() =>
			{
				return targetTile.map.CanCurrentlyBuild (type, targetTile, false);
			});
	}

	private TaskPerformed isJobPerformed;
	private TaskSuccessful jobCompleted;
	private TaskPerformed isJobCurrentlyPossible;
	public string jobType { get; private set; }
	private float totalWorkTime;
	private bool standOnTile;

	public WorkJob (Tile target, string jobType, float totalWorkTime, bool standOnTile, TaskPerformed isTaskPerformed, TaskSuccessful jobCompleted, TaskPerformed isJobCurrentlyPossible) : base (target)
	{
		this.jobCompleted = jobCompleted;
		this.isJobPerformed = isTaskPerformed;
		this.jobType = jobType;
		this.totalWorkTime = totalWorkTime;
		this.standOnTile = standOnTile;
		this.isJobCurrentlyPossible = isJobCurrentlyPossible;
	}

	public override Task[] GetInitialTasks (Entity entity)
	{
		return new Task[]
		{
			new WorkTask (
				targetTile,
				totalWorkTime,
				standOnTile,
				isJobPerformed,
				() =>
				{
					jobCompleted?.Invoke();
					entity.ClearCurrentJob ();
					entity.map.jobManger.RemoveJob (this);
				})
		};
	}

	public override float GetWeight (Entity entity)
	{
		return Node.SqrDistance (entity.currentTile, targetTile);
	}

	public override bool IsSameJob (Job other)
	{
		if (other is WorkJob)
		{
			return targetTile == other.targetTile && jobType == ((WorkJob)other).jobType;
		}

		return false;
	}

	public override bool IsCurrentlyValid (Map map)
	{
		if (!isJobCurrentlyPossible ()) return false;
		foreach (Tile item in map.GetWalkableNeighbours (targetTile)) return true;
		return false;
	}
}
