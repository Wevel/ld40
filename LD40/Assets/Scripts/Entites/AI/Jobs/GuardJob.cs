﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardJob : Job 
{
	public GuardJob (Tile targetTile) : base (targetTile) { }

	public override Task[] GetInitialTasks (Entity entity)
	{
		return new Task[]
		{
			new WaitForEnemyTask (
				targetTile,
				() =>
				{
					entity.ClearCurrentJob ();
					entity.map.jobManger.RemoveJob (this);
				})
		};
	}

	public override float GetWeight (Entity entity)
	{
		return -Node.SqrDistance (entity.currentTile, targetTile);
	}

	public override bool IsSameJob (Job other)
	{
		return false;
	}

	public override bool IsCurrentlyValid (Map map)
	{
		return map.IsWalkable (targetTile);
	}
}
