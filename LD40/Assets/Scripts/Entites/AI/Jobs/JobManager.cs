﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JobManager
{
	public readonly Map map;

	private List<Job> currentJobs = new List<Job> ();
	private List<Job> invalidJobs = new List<Job> ();

	public int JobCount
	{
		get
		{
			return currentJobs.Count + invalidJobs.Count;
		}
	}

	public IEnumerable<Job> ValidJobs
	{
		get
		{
			for (int i = 0; i < currentJobs.Count; i++) yield return currentJobs[i];
		}
	}

	public IEnumerable<Job> InvalidJobs
	{
		get
		{
			for (int i = 0; i < invalidJobs.Count; i++) yield return invalidJobs[i];
		}
	}

	public JobManager (Map map)
	{
		this.map = map;
	}

	public bool TryFindJob (Entity entity, out Job job)
	{
		Job bestJob = null;
		float bestWeight = float.MaxValue;
		float tmpWeight;

		for (int i = 0; i < currentJobs.Count; i++)
		{
			if (IsJobValid (entity, currentJobs[i]))
			{
				tmpWeight = currentJobs[i].GetWeight (entity);
				if (bestJob == null || tmpWeight < bestWeight)
				{
					bestJob = currentJobs[i];
					bestWeight = tmpWeight;
				}
			}
		}

		job = bestJob;

		if (bestJob != null)
		{
			bestJob.TakeJob (entity);
			return true;
		}

		return false;
	}

	public bool IsJobQueued (Job job)
	{
		return currentJobs.Contains (job) || invalidJobs.Contains (job);
	}

	public void QueueJob (Job job)
	{
		if (job.isValid)
		{
			if (HasJobBeenQueues (job)) return;

			if (job.IsCurrentlyValid (map)) currentJobs.Add (job);
			else invalidJobs.Add (job);
		}
		else
		{
			Debug.LogWarning ("JobManager.QueueJob: Job is not valid");
		}
	}

	public bool HasJobBeenQueues (Job job)
	{
		for (int i = 0; i < currentJobs.Count; i++)
		{
			if (currentJobs[i] == job) return true;
		}

		for (int i = 0; i < invalidJobs.Count; i++)
		{
			if (invalidJobs[i] == job) return true;
		}

		return false;
	}

	public bool IsJobValid (Entity entity, Job job)
	{
		return job.assignedWorker == null || job.assignedWorker == entity;
	}

	public void CancelJob (System.Predicate<Job> match)
	{
		for (int i = currentJobs.Count - 1; i >= 0; i--)
		{
			if (match (currentJobs[i]))
			{
				if (currentJobs[i].assignedWorker != null) currentJobs[i].assignedWorker.ClearCurrentJob ();

				currentJobs.RemoveAt (i);
			}
		}

		for (int i = invalidJobs.Count - 1; i >= 0; i--)
		{
			if (match (invalidJobs[i]))
			{
				if (invalidJobs[i].assignedWorker != null) invalidJobs[i].assignedWorker.ClearCurrentJob ();

				invalidJobs.RemoveAt (i);
			}
		}
	}

	public void RemoveJob (Job job)
	{
		currentJobs.Remove (job);
		Update ();
	}

	public void FailJob (Job job)
	{
		if (currentJobs.Contains (job))
		{
			currentJobs.Remove (job);
			invalidJobs.Add (job);
		}
	}

	public void Update ()
	{
		for (int i = invalidJobs.Count - 1; i >= 0; i--)
		{
			if (invalidJobs[i].IsCurrentlyValid (map))
			{
				currentJobs.Add (invalidJobs[i]);
				invalidJobs.RemoveAt (i);
			}
		}
	}
}
