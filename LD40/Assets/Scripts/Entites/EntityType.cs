﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EntityType  : BaseDataType
{
	private static uint nextID = 0;

	public bool isMonster = true;
	public float speed = 5;
	public float maxHealth = 1;
	public float healRate = 1;
	public float healDelay = 1;
	public float damage = 1;
	public Vector2 size;

	public float monsterStrength = 1;

	public override void SetupID ()
	{
		id = nextID;
		nextID++;
	}
}
