﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Entity : Health
{
	public readonly Map map;
	public readonly EntityType type;
	public readonly InterruptibleTaskQueue taskQueue;

	public Tile currentTile { get; private set; }
	public Job currentJob { get; protected set; }

	private bool thrown = false;
	private Vector2 velocity;
	private Vector2 throwTarget;

	protected Entity (Map map, EntityType type, Tile initialTile) : base (type.maxHealth, type.healRate, type.healDelay, type.size, type.displayOffset)
	{
		this.map = map;
		this.type = type;
		this.currentTile = initialTile;
		this.position = new Vector2 (initialTile.x, initialTile.y);
		this.taskQueue = new InterruptibleTaskQueue (this);
	}

	public override void Update (float deltaTime)
	{
		base.Update (deltaTime);

		if (thrown)
		{
			SetPosition (Vector2.SmoothDamp (position, throwTarget, ref velocity, 0.25f, 100, deltaTime));
			if (Vector2.Distance (position, throwTarget) < 0.01f) thrown = false;
		}
		else
		{
			taskQueue.UpdateTasks (deltaTime);
		}
	}

	public void ClearCurrentJob ()
	{
		if (taskQueue.currentTask != null) taskQueue.ClearQueue ();
		currentJob?.LeaveJob (this);
		currentJob = null;
	}

	public void FailCurrentJob ()
	{
		if (currentJob != null)
		{
			map.jobManger.FailJob (currentJob);
			currentJob.LeaveJob (this);
			currentJob = null;
		}
	}

	internal virtual void AttemptSetPosition (Vector2 newPosition)
	{
		SetPosition (newPosition);
	}

	protected void SetPosition (Vector2 newPosition)
	{
		this.position = newPosition;
		currentTile = map.GetTile (Mathf.RoundToInt (newPosition.x), Mathf.RoundToInt (newPosition.y));
	}

	protected Tile GetRandomMoveTarget ()
	{
		List<Tile> possible = new List<Tile> ();
		foreach (Tile item in map.GetWalkableNeighbours (currentTile)) possible.Add (item);

		if (possible.Count > 0) return possible[Random.Range (0, possible.Count)];
		else return currentTile;
	}

	protected override void Die ()
	{
		map.EntityDie (this);
	}

	public void ThrowEntity (Vector2 direction)
	{
		thrown = true;
		velocity = new Vector2 (0, 1) + direction;
		throwTarget = position + direction.normalized;
	}

	public abstract Task[] GetNewTasks ();
}
