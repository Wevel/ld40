﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterManager 
{
	private const float maxTimeBeforeAttacksForEntitySpawn = 20;
	private const float spawnDelay = 0.5f;

	public readonly Map map;

	public bool IsOngoingAttack
	{
		get
		{
			return currentAttackMonsters.Count > 0 || toSpawnMonsters.Count > 0;
		}
	}

	public bool IsAttackSoon
	{
		get
		{
			return attackTimer < maxTimeBeforeAttacksForEntitySpawn || IsOngoingAttack;
		}
	}

	private float attackTimer = 50;

	private int attackCount;

	private List<Monster> currentAttackMonsters = new List<Monster> ();
	private Queue<EntityType> toSpawnMonsters = new Queue<EntityType> ();

	private List<EntityType> monsterTypes;

	private float spawnTimer = 0;

	public MonsterManager (Map map)
	{
		this.map = map;
		monsterTypes = map.GetMonsterTypes ();
	}

	public void Update (float deltaTime)
	{
		if (toSpawnMonsters.Count > 0)
		{
			if (spawnTimer > 0)
			{
				spawnTimer -= deltaTime;
			}
			else
			{
				spawnTimer = spawnDelay;
				Entity entity;
				if (map.TrySpawnEntity (toSpawnMonsters.Dequeue (), map.GetTile (-39, 0), out entity))
				{
					if (entity is Monster) currentAttackMonsters.Add ((Monster)entity);
					else entity.DoDamage (entity.type.maxHealth * 2);
				}
			}
		}

		if (currentAttackMonsters.Count > 0)
		{
			for (int i = currentAttackMonsters.Count - 1; i >= 0; i--)
			{
				if (!currentAttackMonsters[i].IsAlive) currentAttackMonsters.RemoveAt (i);
			}
		}
		else
		{
			if (attackTimer > 0)
			{
				attackTimer -= deltaTime;
			}
			else
			{
				attackTimer = Random.Range (50, 60);
				spawnAttack (getStrength (map.resourceManager.ExactCorruption));
			}
		}
	}

	private void spawnAttack (float strength)
	{
		if (strength > 2)
		{
			attackCount++;
			List<EntityType> stillValid;
			EntityType type;
			while (strength > 0)
			{
				stillValid = monsterTypes.FindAll (x => x.monsterStrength < strength);
				if (stillValid.Count == 0) break;

				type = stillValid[Random.Range (0, stillValid.Count - 1)];
				strength -= type.monsterStrength;
				toSpawnMonsters.Enqueue (type);
			}

			spawnTimer = spawnDelay;
		}
	}

	private float getStrength (float c)
	{
		float divisor = 1;
		if (attackCount < 4) divisor = 5 - attackCount;

		if (c > 0) return (Mathf.Sqrt (c) + (c / 4)) / divisor;
		else return 0;
	}
}
