﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : Entity 
{
	private Entity currentTarget;

	public Monster (Map map, EntityType type, Tile initialTile) : base (map, type, initialTile) { }

	public override Task[] GetNewTasks ()
	{
		Entity closest = null;
		float closestDistance = float.MaxValue;
		float tmpDistance;

		foreach (Entity item in map.Entities)
		{
			if (item is Worker)
			{
				tmpDistance = Vector2.SqrMagnitude (item.position - this.position);
				if (closest == null || tmpDistance < closestDistance)
				{
					closest = item;
					closestDistance = tmpDistance;
				}
			}
		}

		if (closest != null)
		{
			currentTarget = closest;
			return new Task[] { new AttackTask (closest) };
		}
		else
		{
			return new Task[] { new MoveTask (GetRandomMoveTarget ()) };
		}
	}

	internal override void AttemptSetPosition (Vector2 newPosition)
	{
		Vector2 delta = newPosition - position;

		// Check for collision with walls

		if (currentTile.building != null && !currentTile.building.isBlueprint && currentTile.building.type.hasHealth)
		{
			if (this.DoesOverlap (currentTile.building))
			{
				currentTile.building.DoDamage (type.damage);
				ThrowEntity (-delta);
				return;
			}
		}

		List<Entity> possibleCollisionEntities = new List<Entity> ();
		possibleCollisionEntities.AddRange (map.GetEntitiesOnTile (currentTile));

		if (delta.x > 0)
		{
			// Moveing right
			possibleCollisionEntities.AddRange (map.GetEntitiesOnTile (map.GetTile (currentTile.x + 1, currentTile.y)));
		}
		else if (delta.x < 0)
		{
			// Moving left
			possibleCollisionEntities.AddRange (map.GetEntitiesOnTile (map.GetTile (currentTile.x - 1, currentTile.y)));
		}
		else if (delta.y > 0)
		{
			// Moving up
			possibleCollisionEntities.AddRange (map.GetEntitiesOnTile (map.GetTile (currentTile.x, currentTile.y + 1)));
		}
		else if (delta.y < 0)
		{
			// Moving down
			possibleCollisionEntities.AddRange (map.GetEntitiesOnTile (map.GetTile (currentTile.x, currentTile.y - 1)));
		}

		Entity other;
		for (int i = 0; i < possibleCollisionEntities.Count; i++)
		{
			other = possibleCollisionEntities[i];
			if (other is Worker)
			{
				if (this.DoesOverlap (other))
				{
					ThrowEntity (position - other.position);
					this.DoDamage (type.damage);

					if (other.IsAlive)
					{
						other?.ThrowEntity (other.position - position);
						other.DoDamage (type.damage);
					}

					break;
				}
			}
		}

		base.AttemptSetPosition (newPosition);
	}
}
