﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResearchManager
{
	public readonly Map map;

	private List<Research> completedResearch = new List<Research> ();
	private List<Research> remainingResearch = new List<Research> ();

	public Research currentResearch { get; private set; }

	public bool CanResearch
	{
		get
		{
			return currentResearch != null && !currentResearch.hasCompleted;
		}
	}

	public ResearchManager (Map map, List<ResearchType> researchTypes)
	{
		this.map = map;

		for (int i = 0; i < researchTypes.Count; i++) remainingResearch.Add (new Research (map, researchTypes[i]));
	}

	public void DoResearchProgress (float deltaTime)
	{
		if (currentResearch != null)
		{
			if (currentResearch.DoProgress (deltaTime * (1.0f + map.modifierManager.GetModifier("researchSpeed"))))
			{
				completedResearch.Add (currentResearch);
				remainingResearch.Remove (currentResearch);

				for (int i = 0; i < currentResearch.type.completeModifiers.Count; i++)
					map.modifierManager.AddModifier (currentResearch.type.completeModifiers[i]);

				currentResearch = null;
			}
		}
	}

	public List<Research> GetPossibleResearch ()
	{
		return remainingResearch.FindAll (x => x.type.CanStart (completedResearch));
	}

	public void SelectResearch (Research research)
	{
		if (research == null) currentResearch = null;
		else if (research.type.CanStart (completedResearch) && research.TryMakePayment ()) currentResearch = research;
	}
}
