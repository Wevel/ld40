﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResearchType : BaseDataType
{
	private static uint nextID = 0;

	public float totalProgressNeeded;
	public uint goldNeeded = 0;
	public uint gemsNeeded = 0;
	public List<string> prerequisits = new List<string> ();
	public List<Modifier> completeModifiers = new List<Modifier> ();
	
	public override void SetupID ()
	{
		id = nextID;
		nextID++;
	}

	public bool CanStart (List<Research> allResearch)
	{
		Research research;
		for (int i = 0; i < prerequisits.Count; i++)
		{
			if ((research = allResearch.Find (x => x.type.key == prerequisits[i])) == null || !research.hasCompleted) return false;
		}

		return true;
	}
}
