﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Research
{
	public readonly Map map;
	public readonly ResearchType type;
	private float totalProgress = 0;

	public float PercentComplete
	{
		get
		{
			return totalProgress / type.totalProgressNeeded;
		}
	}

	public bool hasCompleted { get; private set; }
	public bool madePayment { get; private set; }

	public Research (Map map, ResearchType type)
	{
		this.map = map;
		this.type = type;
		hasCompleted = false;
	}

	public bool DoProgress (float deltaTime)
	{
		if (!madePayment && !TryMakePayment ()) return false;

		totalProgress += deltaTime;

		if (totalProgress > type.totalProgressNeeded)
		{
			hasCompleted = true;
		}

		return hasCompleted;
	}

	public bool TryMakePayment ()
	{
		if (madePayment) return true;
		if (map.resourceManager.TryUseResearchResources (this)) madePayment = true;

		return madePayment;
	}
}
