﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlacementRestriction 
{
	public int xOffset;
	public int yOffset;
	public bool requireFalse;
	public string targetTileType;

	public bool IsMet (Map map, int baseX, int baseY)
	{
		Tile tile = map.GetTile (baseX + xOffset, baseY + yOffset);

		if (targetTileType.Contains ("building"))
		{
			if (requireFalse) return tile.building != null && !tile.building.isBlueprint && $"building[{tile.building.type.key}]" != targetTileType;
			else return tile.building != null && !tile.building.isBlueprint && $"building[{tile.building.type.key}]" == targetTileType;
		}
		else
		{
			if (requireFalse) return tile.type.key != targetTileType;
			else return tile.type.key == targetTileType;
		}
	}
}
