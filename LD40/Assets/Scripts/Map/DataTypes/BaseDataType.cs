﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class BaseDataType 
{
	public uint id { get; protected set; }
	public string key;
	public string name;
	public Sprite display;
	public Vector3 displayOffset;

	public abstract void SetupID ();
}
