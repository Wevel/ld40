﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuildingType : BaseDataType
{
	private static uint nextID = 0;

	public bool isWalkable;
	public bool mustBeOutside;
	public bool hasAssignedWorler = true;

	public float maxHealth = 1;
	public float healRate = 1;
	public float healDelay = 1;
	public bool hasHealth = false;
	public Vector2 size;

	public List<ResourceCost> buildCost = new List<ResourceCost> ();
	public List<ResourceCost> deconstructReturn = new List<ResourceCost> ();

	public override void SetupID ()
	{
		id = nextID;
		nextID++;
	}
}
