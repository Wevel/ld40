﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DetailType : BaseDataType
{
	private static uint nextID = 0;

	public List<PlacementRestriction> placementRestrictions = new List<PlacementRestriction> ();

	public override void SetupID ()
	{
		id = nextID;
		nextID++;
	}

	public bool IsMet (Map map, int baseX, int baseY)
	{
		for (int i = 0; i < placementRestrictions.Count; i++)
		{
			if (!placementRestrictions[i].IsMet (map, baseX, baseY)) return false;
		}

		return true;
	}
}
