﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TileType : BaseDataType
{
	private static uint nextID = 0;

	public bool isBuildable;
	public bool isWalkable;
	public string mineFinalType;

	public List<ResourceCost> mineProducts = new List<ResourceCost> ();

	public override void SetupID ()
	{
		id = nextID;
		nextID++;
	}
}
