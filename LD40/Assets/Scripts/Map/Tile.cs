﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : Node
{
	public readonly Map map;
	public readonly bool isOutside;

	public TileType type { get; private set; }
	public Building building { get; private set; }

	public Tile (Map map, int x, int y) : base (x, y)
	{
		this.map = map;
		this.isOutside = map.IsOutside (x, y);
		this.type = map.GetDefaultTileType (x, y, isOutside);
	}

	public void SetTileType (TileType newTileType)
	{
		if (newTileType != null)
		{
			type = newTileType;
			map.OnUpdateTile (this);
		}
		else
		{
			Debug.LogWarning ("TIle.SetTileType: There shouldn't be a tile with no type");
		}
	}

	public List<uint> GetDetailTypes ()
	{
		return map.GetDetailTypes (this);
	}

	public void AddBuilding (Building building)
	{
		if (this.building != null)
		{
			Debug.LogWarning ("Tile.AddBuilding: Already have a building set");
			return;
		}

		this.building = building;
		this.building.SetTile (this);
		map.OnUpdateTile (this);
	}

	public void RemoveBuilding ()
	{
		if (building == null)
		{
			Debug.LogWarning ("Tile.AddBuilding: Don't have a building set");
			return;
		}

		building.UnSetTile ();
		map.RemoveBuilding (building);
		building = null;
		map.OnUpdateTile (this);
	}

	public bool IsNeighbour (Tile other)
	{
		return Mathf.Abs (other.x - x) + Mathf.Abs (other.y - y) == 1;
	}
}
