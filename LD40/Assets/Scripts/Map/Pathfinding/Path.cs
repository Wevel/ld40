﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path 
{
	public int Length
	{
		get
		{
			return tiles.Count;
		}
	}

	public Tile FirstTile
	{
		get
		{
			return tiles.Peek ();
		}
	}

	private Queue<Tile> tiles = new Queue<Tile> ();

	public Path ()
	{
		tiles = new Queue<Tile> ();
	}

	public Path (List<Tile> tiles)
	{
		this.tiles = new Queue<Tile> (tiles);
	}

	public void AddTile (Tile tile)
	{
		tiles.Enqueue (tile);
	}

	public Tile RemoveFirstTile ()
	{
		if (tiles.Count > 0) return tiles.Dequeue ();
		else Debug.LogError ("Path.RemoveFirstTile: No tile in path");

		return null;
	}

	public void RemoveAfter (int count)
	{
		int takeCount = Mathf.Min (count, tiles.Count);

		Queue<Tile> shortenedPath = new Queue<Tile> ();
		for (int i = 0; i < takeCount; i++) shortenedPath.Enqueue (tiles.Dequeue ());
		tiles = shortenedPath;
	}
}
