﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Pathfinding 
{
	private class PathNode
	{
		public bool closed = false;
		public float fScore;
		public float gScore;
		public PathNode parent;
		public Tile node;
		public int hashCode;

		public PathNode (Tile node)
		{
			this.node = node;
			this.hashCode = node.hashCode;
		}

		public override int GetHashCode ()
		{
			return hashCode;
		}
	}

	public const float maxPathCheckDistance = 20000;

	public static Path GetPath (Map map, Entity entity, Tile target)
	{
		if (!map.IsWalkable (target))
		{
			Debug.LogWarning ($"Pathfinding.GetPath: Target {target} is not walkable");
			return null;
		}

		List<PathNode> nodes = new List<PathNode> ();
		nodes.Add (new PathNode (entity.currentTile));

		int closedNodes = 0;

		float tgSocre;

		PathNode currentNode = null, existingNode;

		while (nodes.Count > closedNodes)
		{
			currentNode = null;
			foreach (PathNode item in nodes)
			{
				if (!item.closed)
				{
					if (currentNode == null || item.fScore < currentNode.fScore) currentNode = item;
				}
			}

			if (currentNode.node == target) break;
			currentNode.closed = true;
			closedNodes++;

			if (currentNode.fScore > maxPathCheckDistance) continue;

			foreach (Tile item in map.GetWalkableNeighbours (currentNode.node))
			{
				if ((existingNode = nodes.Find (x => x.node == item)) != null)
				{
					if (existingNode.closed) continue;
					tgSocre = currentNode.gScore + getMoveCost (currentNode.node, item);
					if (existingNode.gScore > tgSocre) continue;
				}
				else
				{
					tgSocre = currentNode.gScore + getMoveCost (currentNode.node, item);

					// As the map is infinite, this will stop the algorimth running for ever

					existingNode = new PathNode (item);
					nodes.Add (existingNode);
				}

				existingNode.parent = currentNode;
				existingNode.gScore = tgSocre;
				existingNode.fScore = tgSocre + estimateDistance (item, target);
			}
		}

		if (currentNode.node != target)
		{
			Debug.LogWarning ($"Pathfinding.GetPath: Couldn't find path from {entity.currentTile} to {target}");
			return null;
		}

		List<Tile> path = new List<Tile> ();
		path.Add (currentNode.node);

		while ((currentNode = currentNode.parent) != null) path.Add (currentNode.node);

		path.Reverse ();

		return new Path (path);
	}

	private static float getMoveCost (Tile a, Tile b)
	{
		return Node.SqrDistance (a, b);
	}

	private static float estimateDistance (Tile a, Tile target)
	{
		return Node.SqrDistance (a, target);
	}
}
