﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk
{
	public static int CalculateHashCode (int cx, int cy)
	{
		return cx | (cy << 16);
	}

	public readonly Map map;
	public readonly int size;

	public readonly int chunkX;
	public readonly int chunkY;
	public readonly int baseX;
	public readonly int baseY;

	public readonly int hashCode;

	private Tile[,] tiles;

	public Chunk (Map map, int size, int chunkX, int chunkY)
	{
		this.map = map;
		this.size = size;
		this.chunkX = chunkX;
		this.chunkY = chunkY;
		this.baseX = size * chunkX;
		this.baseY = size * chunkY;

		hashCode = CalculateHashCode (chunkX, chunkY);

		// Setup the tiles for the chunk
		tiles = new Tile[size, size];

		List<Tile> outsideTiles = new List<Tile> ();
		List<Tile> resourceTiles = new List<Tile> ();

		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < size; y++)
			{
				tiles[x, y] = new Tile (map, baseX + x, baseY + y);
				if (tiles[x, y].isOutside && baseY + y == 0 && baseX + x < 0) outsideTiles.Add (tiles[x, y]);
				else if (tiles[x, y].type != null && tiles[x, y].type.key == "goldOre") resourceTiles.Add (tiles[x, y]);
			}
		}

		map.SpawnOutsideBuildingsForChunk (outsideTiles);
		map.SetResourceTypes (this, resourceTiles);
	}

	public Tile GetTile (int x, int y)
	{
		int lx = x - baseX;
		int ly = y - baseY;

		if (lx >= 0 && lx < size && ly >= 0 && ly < size) return tiles[lx, ly];

		Debug.LogError ($"Chunk.GetTile: Tile is not on this chunk {x}, {y}");
		return null;
	}

	public override int GetHashCode ()
	{
		return hashCode;
	}
}
