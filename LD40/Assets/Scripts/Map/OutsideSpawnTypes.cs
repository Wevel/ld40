﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class OutsideSpawnTypes  
{
	public float spawnChance;
	public List<string> buildingTypes = new List<string> ();

	public string GetBuildingType (System.Random rnd)
	{
		return buildingTypes[rnd.Next (buildingTypes.Count)];
	}
}
