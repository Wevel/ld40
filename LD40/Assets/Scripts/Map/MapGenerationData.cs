﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapGenerationData  
{
	public uint mountainEdgeNoiseOctaves = 3;
	public float mountainEdgeNoiseFrequency = 0.01f;
	public float mountainEdgeNoisePersistence = 0.75f;
	public int mountainEdgeVariance = 5;

	public uint resourceTypeNoiseOctaves = 2;
	public float resourceTypeNoiseFrequency = 0.1f;
	public float resourceTypeNoisePersistence = 0.5f;
	public float resourceTypeCuttoff = 0.8f;

	public int maxResourceTypeGroupSize = 8;
}
