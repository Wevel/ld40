﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node 
{
	public readonly int x;
	public readonly int y;

	public readonly int hashCode;

	public Node (int x, int y)
	{
		this.x = x;
		this.y = y;
		hashCode = x | (y << 16);
	}

	public override string ToString ()
	{
		return $"[{x}, {y}]";
	}

	public static float Distance (Node a, Node b)
	{
		return Mathf.Sqrt (SqrDistance (a, b));
	}

	public static float Distance (Vector2 a, Node b)
	{
		return Mathf.Sqrt (SqrDistance (a, b));
	}

	public static float SqrDistance (Node a, Node b)
	{
		float x = b.x - a.x;
		float y = b.y - a.y;

		return (x * x) + (y * y);
	}

	public static float SqrDistance (Vector2 a, Node b)
	{
		float x = b.x - a.x;
		float y = b.y - a.y;

		return (x * x) + (y * y);
	}
}
