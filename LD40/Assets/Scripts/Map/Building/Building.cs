﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : Health
{
	public readonly Map map;
	public readonly BuildingType type;
	public readonly bool isBlueprint;

	public Tile tile { get; private set; }

	public Worker assignedEntity { get; private set; }

	public Building (Map map, BuildingType type, bool isBluePrint) : base (type.maxHealth, type.healRate, type.healDelay, type.size, type.displayOffset)
	{
		this.map = map;
		this.type = type;
		this.isBlueprint = isBluePrint;
	}

	public override void Update (float deltaTime)
	{
		base.Update (deltaTime);
		if (type.hasAssignedWorler && (assignedEntity == null || !assignedEntity.IsAlive)) map.SpawnEntityForBuilding (this);
	}

	public void SetTile (Tile tile)
	{
		if (this.tile != null)
		{
			Debug.LogWarning ("Building.SetTile: Already have a tile set");
			return;
		}

		this.tile = tile;
		this.position = new Vector2 (tile.x, tile.y);
	}

	public void UnSetTile ()
	{
		if (tile == null)
		{
			Debug.LogWarning ("Building.UnSetTile: No tile set");
			return;
		}

		tile = null;
	}

	public bool IsWalkable
	{
		get
		{
			if (isBlueprint) return true;
			else return type.isWalkable;
		}
	}

	public void AssignWorker (Worker entity)
	{
		if (assignedEntity != null) assignedEntity.UnAssignBuilding ();
		assignedEntity = entity;
		if (assignedEntity != null) assignedEntity.AssignBuilding (this);
	}

	protected override void Die ()
	{
		tile.RemoveBuilding ();
	}
}
