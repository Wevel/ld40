﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityControl : MonoBehaviour 
{
	public Transform entityHolder;
	public Material entityMaterial;
	public float zOffset;
	
	private Map _map;
	public Map map
	{
		get
		{
			return _map;
		}
		set
		{
			if (_map != null)
			{
				_map.onEntityAdded -= OnEntityAdded;
				_map.onEntityDie -= OnEntityDie;
			}

			_map = value;
			if (_map != null)
			{
				_map.onEntityAdded += OnEntityAdded;
				_map.onEntityDie += OnEntityDie;
			}
		}
	}

	private Dictionary<Entity, EntityRenderer> currentRenderers = new Dictionary<Entity, EntityRenderer> ();
	private Queue<EntityRenderer> rendererPool = new Queue<EntityRenderer> ();

	public void Clear ()
	{
		foreach (KeyValuePair<Entity, EntityRenderer> item in currentRenderers)
		{
			rendererPool.Enqueue (item.Value);
			item.Value.Hide ();
		}
	}

	public void OnEntityAdded (Entity entity)
	{
		// Just make sure that we dont already have the entity
		EntityRenderer er;
		if (currentRenderers.TryGetValue (entity, out er))
		{
			Debug.LogError ("EntityControl.OnAddEntity: Already got entity");
			return;
		}

		er = getEntityRenderer ();
		er.Show (entity);

		currentRenderers.Add (entity, er);
	}

	public void OnEntityDie (Entity entity)
	{
		// Only need to check this if there is an entity to remove
		EntityRenderer er;
		if (currentRenderers.TryGetValue (entity, out er))
		{
			rendererPool.Enqueue (er);
			er.Hide ();
		}
	}

	private EntityRenderer getEntityRenderer ()
	{
		if (rendererPool.Count > 0) return rendererPool.Dequeue ();

		GameObject go = new GameObject ("EntityRenderer");
		go.transform.SetParent (entityHolder);
		go.transform.position = Vector3.zero;

		SpriteRenderer sr = go.AddComponent<SpriteRenderer> ();
		sr.material = entityMaterial;

		EntityRenderer er = go.AddComponent<EntityRenderer> ();
		er.zOffset = zOffset;
		er.Hide ();

		return er;
	}
}
