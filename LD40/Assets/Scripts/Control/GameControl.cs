﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.EventSystems;

public class GameControl : MonoBehaviour 
{
	public static int totalGames = 0;

	public int mapSeed;
	public int chunksSize = 16;
	public MapGenerationData mapGenerationData;

	public ResourceUI resourceUI;
	public WinLose winLose;
	public QueuedJobsDisplay jobDisplay;

	public List<TileType> tileTypes = new List<TileType> ();
	public List<BuildingType> buildingTypes = new List<BuildingType> ();
	public List<OutsideSpawnTypes> outsideBuildingTypes = new List<OutsideSpawnTypes> ();
	public List<EntityType> entityTypes = new List<EntityType> ();
	public List<ResourceType> resourceTypes = new List<ResourceType> ();
	public List<DetailType> detailTypes = new List<DetailType> ();
	public List<ResearchType> researchTypes = new List<ResearchType> ();

	public Map currentMap { get; private set; }

	public SpriteRenderer currentBuildPlacer;

	public Color canBuildPlacerColour = Color.white;
	public Color cantBuildPlacerColour = Color.white;

	public Sprite removerToolSprite;

	private MapRenderer mr;
	private EntityControl ec;

	public int currentX;
	public int currentY;
	public string tileType;

	public int totalJobs;

	public BuildingType currentBuildingType { get; private set; }
	public bool removeTool { get; private set; }

	private void Awake ()
	{
		this.mr = GetComponent<MapRenderer> ();
		this.ec = GetComponent<EntityControl> ();
		this.jobDisplay = GetComponent<QueuedJobsDisplay> ();
		mr.chunkSize = chunksSize;
		currentBuildingType = null;

		for (int i = 0; i < tileTypes.Count; i++) tileTypes[i].SetupID ();
		for (int i = 0; i < buildingTypes.Count; i++) buildingTypes[i].SetupID ();
		for (int i = 0; i < entityTypes.Count; i++) entityTypes[i].SetupID ();
		for (int i = 0; i < detailTypes.Count; i++) detailTypes[i].SetupID ();
	}

	private void Update ()
	{
		if (currentMap != null)
		{
			if (Application.isEditor)
			{
				if (Input.GetMouseButtonUp (3))
				{
					onWin ();
					return;
				}

				if (Input.GetMouseButtonUp (4))
				{
					onLose ();
					return;
				}
			}

			currentMap.Update (Time.deltaTime);

			// Check if we have won, the win modifier should be set to a fairly large number
			if (currentMap.modifierManager.GetModifier ("win") > 420)
			{
				onWin ();
			}
			else
			{
				totalJobs = currentMap.jobManger.JobCount;

				Vector2 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				int x = Mathf.RoundToInt (pos.x);
				int y = Mathf.RoundToInt (pos.y);
				currentX = x;
				currentY = y;

				Tile tile = currentMap.GetTile (x, y);
				tileType = tile.type != null ? tile.type.name : "Empty";

				currentBuildPlacer.transform.position = new Vector3 (x, y);

				if (!EventSystem.current.IsPointerOverGameObject () && x > -40)
				{
					if (currentBuildingType != null)
					{
						if (currentMap.CanCurrentlyBuild (currentBuildingType, tile, false)) currentBuildPlacer.color = canBuildPlacerColour;
						else currentBuildPlacer.color = cantBuildPlacerColour;

						if (Input.GetMouseButton (0))
						{
							Job job;
							if ((job = WorkJob.BuildJob (tile, currentBuildingType, 1)) != null && job.isValid) currentMap.jobManger.QueueJob (job);
						}
						else if (Input.GetMouseButton (1))
						{
							currentBuildingType = null;
							currentBuildPlacer.sprite = null;
							currentBuildPlacer.gameObject.SetActive (false);
							removeTool = false;
						}
					}
					else
					{
						currentBuildPlacer.color = canBuildPlacerColour;

						if (removeTool)
						{
							if (Input.GetMouseButton (0))
							{
								if (Input.GetKey (KeyCode.LeftAlt) || Input.GetKey (KeyCode.RightAlt))
								{
									currentMap.jobManger.CancelJob (a => a is WorkJob && (((WorkJob)a).jobType == $"mineJob{tile}" || ((WorkJob)a).jobType == $"buildJob{tile}" || ((WorkJob)a).jobType == $"deconstructJob{tile}"));
								}
								else
								{
									if (tile.building != null && tile.building.isBlueprint)
									{
										tile.RemoveBuilding ();
										currentMap.jobManger.CancelJob (a => a is WorkJob && ((WorkJob)a).jobType == $"buildJob{tile}");
									}
									else
									{
										Job job;
										if ((job = WorkJob.MineJob (tile, 1)) != null && job.isValid) currentMap.jobManger.QueueJob (job);
										else if ((job = WorkJob.DeconstructJob (tile, 2)) != null && job.isValid) currentMap.jobManger.QueueJob (job);
									}
								}
							}
							else if (Input.GetMouseButton (1))
							{			
								currentBuildingType = null;
								currentBuildPlacer.sprite = null;
								currentBuildPlacer.gameObject.SetActive (false);
								removeTool = false;
							}
						}
					}
				}
			}
		}
	}

	public void StartNewMap ()
	{
		Debug.Log ("GameControl.StartNewMap: Starting new map");

		Clear ();

		currentMap = new Map (
			mapSeed, 
			chunksSize, 
			mapGenerationData,
			tileTypes, 
			buildingTypes, 
			outsideBuildingTypes,
			entityTypes,
			resourceTypes,
			detailTypes,
			researchTypes);

		currentMap.onLose += onLose;

		mr.map = currentMap;
		ec.map = currentMap;
		resourceUI.map = currentMap;

		for (int i = 0; i < tileTypes.Count; i++) mr.AddTileType (tileTypes[i]);
		for (int i = 0; i < detailTypes.Count; i++) mr.AddDetailType (detailTypes[i]);
		for (int i = 0; i < buildingTypes.Count; i++) mr.AddBuildingType (buildingTypes[i]);

		Tile portalTile = currentMap.GetTile (-40, 0);
		if (portalTile.building != null) portalTile.RemoveBuilding ();
		Debug.Assert (currentMap.TryBuild (currentMap.GetBuildingType ("portal"), portalTile, false), "GameControl.StartNewMap: Couldn't place portal");

		Tile initialWorkerTools = currentMap.GetTile (4, 0);
		initialWorkerTools.SetTileType (currentMap.GetTileType("stoneFloor"));
		Debug.Assert (currentMap.TryBuild (currentMap.GetBuildingType ("workerTools"), initialWorkerTools, false), "GameControl.StartNewMap: Couldn't place workers tools");

		MainMenu.Paused = false;

		Analytics.CustomEvent ("gameStarted");
		totalGames++;
	}

	public void Clear ()
	{
		mr.map = null;
		ec.map = null;
		resourceUI.map = null;
		currentMap = null;

		mr.Clear ();
		ec.Clear ();
		resourceUI.Clear ();
		jobDisplay.Clear ();
	}

	public void SetBuildingType (BuildingType type)
	{
		currentBuildingType = type;

		if (currentBuildingType != null) currentBuildPlacer.sprite = currentBuildingType.display;
		else currentBuildPlacer.sprite = null;

		currentBuildPlacer.gameObject.SetActive (currentBuildingType != null);
		removeTool = false;
	}

	public void SetRemoveTool (bool value)
	{
		removeTool = value;

		currentBuildPlacer.sprite = removerToolSprite;
		currentBuildPlacer.gameObject.SetActive (value);
	}

	private void onWin ()
	{
		winLose.Win ();

		Analytics.CustomEvent ("gameWon", new Dictionary<string, object>
		{
			{ "seed", currentMap.seed },
			{ "finalCorruption", currentMap.resourceManager.GetResourceCount(ResourceManager.corruptionResourceKey) },
		});
	}

	private void onLose ()
	{
		winLose.Lose ();

		Analytics.CustomEvent ("gameLost", new Dictionary<string, object>
		{
			{ "seed", currentMap.seed },
			{ "finalCorruption", currentMap.resourceManager.GetResourceCount(ResourceManager.corruptionResourceKey) },
		});
	}
}
