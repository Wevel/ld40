﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceTypeUI : MonoBehaviour 
{
	public Image icon;
	public Text resourceName;
	public Text countText;

	public void Show (ResourceType type, uint count, uint currentCost, bool showName)
	{
		icon.sprite = type.icon;
		resourceName.text = showName ? $"{type.name}:" : "";

		if (currentCost > 0)
		{
			countText.text = count + " - " + currentCost;
			if (count < currentCost) countText.color = Color.red;
			else countText.color = Color.white;
		}
		else
		{
			countText.text = count + "";
			countText.color = Color.white;
		}

		gameObject.SetActive (true);
	}

	public void Hide ()
	{
		gameObject.SetActive (false);
	}
}
