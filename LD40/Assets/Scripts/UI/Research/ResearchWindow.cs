﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResearchWindow : MonoBehaviour 
{
	public float techWindowScrollSpeed = 10;

	public GameControl control;
	public GameObject windowObject;
	public Transform techTypesHolder;

	public ResearchTypeUI researchTypeUIPrefab;

	private Dictionary<Research, ResearchTypeUI> currentResearchTypes = new Dictionary<Research, ResearchTypeUI> ();
	private Queue<ResearchTypeUI> researchTypePool = new Queue<ResearchTypeUI> ();

	public Transform currentResearchProgress;
	public Text currentResearchName;

	private void Awake ()
	{
		windowObject.SetActive (false);
	}

	private void Update ()
	{
		if (Input.GetButtonDown ("techWindow")) ToggleTechWindow ();

		if (control.currentMap != null)
		{
			if (control.currentMap.researchManager.currentResearch != null)
			{
				currentResearchProgress.localScale = new Vector3 (control.currentMap.researchManager.currentResearch.PercentComplete, 1, 1);
				currentResearchName.text = $"Researching: {control.currentMap.researchManager.currentResearch.type.name}";
			}
			else
			{
				currentResearchProgress.localScale = new Vector3 (0, 1, 1);
				currentResearchName.text = $"Select research by clicking bar above or pressing T";
			}

			if (windowObject.activeSelf)
			{
				foreach (KeyValuePair<Research, ResearchTypeUI> item in currentResearchTypes) item.Value.shouldClose = true;

				List<Research> research = control.currentMap.researchManager.GetPossibleResearch ();

				ResearchTypeUI typeUI;
				foreach (Research item in research)
				{
					if (!currentResearchTypes.TryGetValue (item, out typeUI))
					{
						typeUI = getResearchTypeUI ();
						currentResearchTypes.Add (item, typeUI);
					}

					typeUI.Show (this, item);
					typeUI.shouldClose = false;
				}

				List<Research> toRemove = new List<Research> ();
				foreach (KeyValuePair<Research, ResearchTypeUI> item in currentResearchTypes)
				{
					if (item.Value.shouldClose)
					{
						toRemove.Add (item.Key);
						researchTypePool.Enqueue (item.Value);
						item.Value.Hide ();
					}
				}

				for (int i = 0; i < toRemove.Count; i++) currentResearchTypes.Remove (toRemove[i]);

				techTypesHolder.Translate (0, Input.GetAxis ("Mouse ScrollWheel") * techWindowScrollSpeed * Time.deltaTime, 0, Space.World);
			}
		}
	}

	public void ToggleTechWindow ()
	{
		techTypesHolder.localPosition = Vector3.zero;
		windowObject.SetActive (!windowObject.activeSelf);
		MainMenu.Paused = windowObject.activeSelf;
		MainMenu.ShowingTech = windowObject.activeSelf;
	}

	private ResearchTypeUI getResearchTypeUI ()
	{
		if (researchTypePool.Count > 0) return researchTypePool.Dequeue ();
		return Instantiate (researchTypeUIPrefab, techTypesHolder, false);
	}
}
