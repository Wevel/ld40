﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResearchTypeUI : MonoBehaviour 
{
	public Image researchIcon;
	public Text researchNameText;
	public Text researchGoldCost;
	public Text researchGemsCost;
	public Transform progressTransform;

	public bool shouldClose { get; set; }

	private Research currentResearch = null;
	private ResearchWindow window;

	private void Update () 
	{
		if (currentResearch != null)
		{
			progressTransform.localScale = new Vector3 (currentResearch.PercentComplete, 1, 1);
		}
	}

	public void Show (ResearchWindow window, Research research)
	{
		this.window = window;
		currentResearch = research;

		researchIcon.sprite = research.type.display;
		researchNameText.text = research.type.name;
		researchGoldCost.text = research.type.goldNeeded + "";
		researchGemsCost.text = research.type.gemsNeeded + "";

		gameObject.SetActive (true);
	}

	public void Hide ()
	{
		window = null;
		currentResearch = null;
		gameObject.SetActive (false);
	}

	public void ButtonPressed ()
	{
		if (window != null)
		{
			window.control.currentMap.researchManager.SelectResearch (currentResearch);
			window.ToggleTechWindow ();
		}
	}
}
