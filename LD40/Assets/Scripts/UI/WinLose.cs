﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLose : MonoBehaviour 
{
	public Image winBackground;
	public Text winMainText;
	public Text winInfoText;

	public Image loseBackground;
	public Text loseMainText;
	public Text loseInfoText;

	public float showTime = 3;

	private Coroutine showCoroutine;

	public void Win ()
	{
		MainMenu.Paused = true;
		MainMenu.ShowingTech = true;
		Clear ();
		showCoroutine = StartCoroutine (showScreen (winBackground, winMainText, winInfoText, showTime));
	}

	public void Lose ()
	{
		MainMenu.Paused = true;
		MainMenu.ShowingTech = true;
		Clear ();
		showCoroutine = StartCoroutine (showScreen (loseBackground, loseMainText, loseInfoText, showTime));
	}

	public void Clear ()
	{
		if (showCoroutine != null) StopCoroutine (showCoroutine);
		showCoroutine = null;

		winBackground.gameObject.SetActive (false);
		loseBackground.gameObject.SetActive (false);
	}

	public void ScreenClicked ()
	{
		Clear ();
		MainMenu.ShowMainMenu ();
	}

	private IEnumerator showScreen (Image background, Text mainText, Text infoText, float time)
	{
		Color backgroundColour = background.color;
		Color mainTextColour = mainText.color;
		Color infoTextColour = infoText.color;

		backgroundColour.a = 0;
		mainTextColour.a = 0;
		infoTextColour.a = 0;

		background.color = backgroundColour;
		mainText.color = mainTextColour;
		infoText.color = infoTextColour;

		background.gameObject.SetActive (true);

		float t = 0;

		while (t < 1)
		{
			backgroundColour.a = t;
			mainTextColour.a = t;
			infoTextColour.a = t;

			background.color = backgroundColour;
			mainText.color = mainTextColour;
			infoText.color = infoTextColour;

			t += Time.deltaTime / time;
			yield return null;
		}

		backgroundColour.a = 1;
		mainTextColour.a = 1;
		infoTextColour.a = 1;

		background.color = backgroundColour;
		mainText.color = mainTextColour;
		infoText.color = infoTextColour;
	}
}
