﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundControl : MonoBehaviour 
{
	public float scale = 2;
	public float yValue = -0.5f;

	private SpriteRenderer sr;

	private void Awake ()
	{
		sr = GetComponent<SpriteRenderer> ();
	}

	private void Update ()
	{
		sr.enabled = MainMenu.InGame;
		float x = Camera.main.transform.position.x - (Camera.main.orthographicSize * Camera.main.aspect) - 3;
		transform.position = new Vector3 (Mathf.RoundToInt (x / scale) * scale, yValue);
		sr.size = new Vector2 (scale *  Mathf.Round((Camera.main.orthographicSize * 2) + 2), scale);
	}
}
	