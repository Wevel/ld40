﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class MainMenu : MonoBehaviour
{
	public static bool Paused = false;
	public static bool InGame = false;
	public static bool ShowingTech = false;

	public static void ShowMainMenu ()
	{
		mainMenu.showMainMenu ();
	}

	public GameControl control;

	private static MainMenu mainMenu;

	private bool showPauseMenu = false;

	public GameObject mainMenuObject;
	public GameObject mainMenuBackground;
	public GameObject startButton;
	public GameObject startNewGameButton;
	public GameObject resumeButton;
	public GameObject pauseButton;
	public GameObject exitButton;

	private void Awake ()
	{
		mainMenu = this;

		Analytics.enabled = true;
	}

	private void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			if (Paused) Resume ();
			else ShowPauseMenu ();
		}

		mainMenuObject.SetActive (!InGame || (Paused && !ShowingTech));
		mainMenuBackground.SetActive (!InGame);

		startButton.SetActive (!InGame);
		startNewGameButton.SetActive (Paused && InGame);
		resumeButton.SetActive (Paused && InGame);
		pauseButton.SetActive (!Paused && InGame);
		exitButton.SetActive (!Application.isWebPlayer && !Application.isEditor);
	}

	public void showMainMenu ()
	{
		control.Clear ();
		InGame = false;
		showPauseMenu = false;
		ShowingTech = false;
	}

	public void StartGame ()
	{
		control.mapSeed = Random.Range (0, int.MaxValue);
		control.StartNewMap ();
		InGame = true;
		showPauseMenu = false;
	}

	public void ShowPauseMenu ()
	{
		Paused = true;
		showPauseMenu = true;
	}

	public void Resume ()
	{
		Paused = false;
		showPauseMenu = false;
	}

	public void Exit ()
	{
		Application.Quit ();
	}

	private void OnApplicationQuit ()
	{
		Analytics.CustomEvent ("exit", new Dictionary<string, object>
		{
			{ "totalTime", Time.time },
			{ "totalGames", GameControl.totalGames }
		});

		Analytics.FlushEvents ();
	}
}
