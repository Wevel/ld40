﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingTypeUI : MonoBehaviour 
{
	public Color notSelectedColour = Color.white;
	public Color selectedColour = Color.white;

	public Image iconImage;
	public Text nameText;

	private BuildingTypeSelector selector;
	private BuildingType type;

	private int index = -1;

	private void Update ()
	{
		if (index >= 0 && !MainMenu.Paused)
		{
			if (Input.GetButtonDown ($"building{index}")) ButtonPressed ();
		}
	}

	public void Show (BuildingTypeSelector selector, BuildingType type, int index)
	{
		this.selector = selector;
		this.type = type;
		this.index = index;

		iconImage.sprite = type.display;
		nameText.text = type.name;

		if (selector.control.currentBuildingType == type) iconImage.color = selectedColour;
		else iconImage.color = notSelectedColour;

		gameObject.SetActive (true);
	}

	public void Hide ()
	{
		type = null;
		index = -1;
		iconImage.color = notSelectedColour;
		gameObject.SetActive (false);
	}

	public void ButtonPressed ()
	{
		if (selector != null)
		{
			if (selector.control.currentBuildingType != type) selector.control.SetBuildingType (type);
			else selector.control.SetBuildingType (null);
		}
	}
}
