﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingTypeSelector : MonoBehaviour 
{
	public GameControl control;

	public BuildingTypeUI buildingTypeUIPrefab;

	public List<string> hiddelBuildingSelectButtons = new List<string> ();

	private Dictionary<BuildingType, BuildingTypeUI> currentBuildingTypes = new Dictionary<BuildingType, BuildingTypeUI> ();
	private Queue<BuildingTypeUI> resourceTypePool = new Queue<BuildingTypeUI> ();

	public Transform removeToolButton;
	public Image removeToolImage;

	public Text currentTileTypeText;

	private void Update ()
	{
		if (control.currentMap != null && !MainMenu.Paused)
		{
			Vector2 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			int x = Mathf.RoundToInt (pos.x);
			int y = Mathf.RoundToInt (pos.y);
			Tile tile = control.currentMap.GetTile (x, y);

			if (tile.building != null && !tile.building.isBlueprint) currentTileTypeText.text = $"{tile.type?.name} - {tile.building.type.name}";
			else currentTileTypeText.text = tile.type?.name;

			int i = 1;

			BuildingTypeUI typeUI;
			foreach (BuildingType item in control.currentMap.BuildingTypes)
			{
				if (!hiddelBuildingSelectButtons.Contains (item.key))
				{
					if (!currentBuildingTypes.TryGetValue (item, out typeUI))
					{
						typeUI = getBuildingTypeUI ();
						currentBuildingTypes.Add (item, typeUI);
					}

					typeUI.Show (this, item, i);
					i++;
				}
			}

			if (Input.GetButtonDown ("ClearTile")) RemoveButtonPressed ();

			removeToolButton.SetSiblingIndex (transform.childCount - 1);
			if (control.removeTool) removeToolImage.color = buildingTypeUIPrefab.selectedColour;
			else removeToolImage.color = buildingTypeUIPrefab.notSelectedColour;
		}
	}

	public void Clear ()
	{
		if (currentBuildingTypes.Count > 0)
		{
			foreach (KeyValuePair<BuildingType, BuildingTypeUI> item in currentBuildingTypes)
			{
				item.Value.Hide ();
				resourceTypePool.Enqueue (item.Value);
			}
			currentBuildingTypes.Clear ();
		}
	}

	private BuildingTypeUI getBuildingTypeUI ()
	{
		if (resourceTypePool.Count > 0) return resourceTypePool.Dequeue ();
		return Instantiate (buildingTypeUIPrefab, transform, false);
	}

	public void RemoveButtonPressed ()
	{
		control.SetRemoveTool (!control.removeTool);
	}
}
