﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueuedJobsDisplay : MonoBehaviour 
{
	public Transform displayHolder;
	public Color validDisplayColour;
	public Color invalidDisplayColour;
	public Sprite displaySprite;

	private Dictionary<Job, SpriteRenderer> jobDisplays = new Dictionary<Job, SpriteRenderer> ();
	private Queue<SpriteRenderer> spriteRendeerPool = new Queue<SpriteRenderer> ();

	public GameControl control;

	private void Update ()
	{
		if (control.currentMap != null)
		{
			List<Job> toRemove = new List<Job> ();
			foreach (KeyValuePair<Job, SpriteRenderer> item in jobDisplays)
			{
				if (!control.currentMap.jobManger.IsJobQueued (item.Key))
				{
					toRemove.Add (item.Key);
					item.Value.gameObject.SetActive (false);
					spriteRendeerPool.Enqueue (item.Value);
				}
			}

			for (int i = 0; i < toRemove.Count; i++) jobDisplays.Remove (toRemove[i]);

			SpriteRenderer sr;
			WorkJob workJob;
			foreach (Job item in control.currentMap.jobManger.ValidJobs)
			{
				if (item is WorkJob)
				{
					workJob = (WorkJob)item;

					if (workJob.jobType.Contains ("deconstructJob") || workJob.jobType.Contains ("mineJob"))
					{
						if (!jobDisplays.TryGetValue (item, out sr))
						{
							sr = getSpriteRenderer ();
							jobDisplays.Add (item, sr);
						}

						sr.color = validDisplayColour;
						sr.gameObject.SetActive (true);
						sr.transform.position = new Vector3 (item.targetTile.x, item.targetTile.y, -5);
					}
				}
			}

			foreach (Job item in control.currentMap.jobManger.InvalidJobs)
			{
				if (item is WorkJob)
				{
					workJob = (WorkJob)item;

					if (workJob.jobType.Contains ("deconstructJob") || workJob.jobType.Contains ("mineJob"))
					{
						if (!jobDisplays.TryGetValue (item, out sr))
						{
							sr = getSpriteRenderer ();
							jobDisplays.Add (item, sr);
						}

						sr.color = invalidDisplayColour;
						sr.gameObject.SetActive (true);
						sr.transform.position = new Vector3 (item.targetTile.x, item.targetTile.y, -5);
					}
				}
			}
		}
	}

	public void Clear ()
	{
		foreach (KeyValuePair<Job, SpriteRenderer> item in jobDisplays)
		{
			spriteRendeerPool.Enqueue (item.Value);
		}

		jobDisplays.Clear ();
	}

	public SpriteRenderer getSpriteRenderer ()
	{
		if (spriteRendeerPool.Count > 0) return spriteRendeerPool.Dequeue ();

		GameObject go = new GameObject ("JobDisplay");
		go.transform.SetParent (displayHolder);

		SpriteRenderer sr = go.AddComponent<SpriteRenderer> ();
		sr.sprite = displaySprite;
		sr.color = validDisplayColour;

		return sr;
	}
}
