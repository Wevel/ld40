﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour 
{
	public float keyBoardPanSpeed = 5;
	public float zoomSpeed = 1;

	public float minZoom = 1;
	public float maxZoom = 150;

	private Vector3 lastPosition;

	public Camera mapCamera;
	public Camera backGroundMapCamera;

	private void Update () 
	{
		Camera mainCamera = Camera.main;

		if (mainCamera != null && !MainMenu.Paused)
		{
			if (mainCamera.pixelRect.Contains (Input.mousePosition))
			{
				Vector3 currentPosition = mainCamera.ScreenToWorldPoint (Input.mousePosition);

				if (Input.GetMouseButtonDown (2)) lastPosition = currentPosition;

				if (Input.GetMouseButton (2))
				{
					// Pan
					mainCamera.transform.Translate (lastPosition - currentPosition, Space.World);
				}
				else
				{
					// Zoom
					mainCamera.orthographicSize -= mainCamera.orthographicSize * Input.GetAxis ("Mouse ScrollWheel") * zoomSpeed;

					// Clamp zoom and also move the camera towards the mouse mosition
					if (mainCamera.orthographicSize <= minZoom) mainCamera.orthographicSize = minZoom;
					else if (mainCamera.orthographicSize > maxZoom) mainCamera.orthographicSize = maxZoom;
					else mainCamera.transform.Translate ((Vector2)(currentPosition - mainCamera.transform.position) * Input.GetAxis ("Mouse ScrollWheel"), Space.World);
				}

				lastPosition = mainCamera.ScreenToWorldPoint (Input.mousePosition);
			}

			// Allow panning the camera with the keyboard
			if (!Input.GetMouseButton (2))
			{
				mainCamera.transform.Translate (
					Input.GetAxis ("Horizontal") * keyBoardPanSpeed * mainCamera.orthographicSize * Time.deltaTime,
					Input.GetAxis ("Vertical") * keyBoardPanSpeed * mainCamera.orthographicSize * Time.deltaTime,
					0,
					Space.World);
			}

			mapCamera.orthographicSize = mainCamera.orthographicSize;
			backGroundMapCamera.orthographicSize = mainCamera.orthographicSize;
		}
	}
}
