﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceUI : MonoBehaviour 
{
	public Map map;
	public bool showNames;

	public GameControl control;

	public ResourceTypeUI resourceUIPrefab;

	private Dictionary<ResourceType, ResourceTypeUI> currentResourceTypes = new Dictionary<ResourceType, ResourceTypeUI> ();
	private Queue<ResourceTypeUI> resourceTypePool = new Queue<ResourceTypeUI> ();

	private void Awake ()
	{
		control.resourceUI = this;
	}

	private void Update ()
	{
		if (map != null)
		{
			ResourceTypeUI typeUI;
			ResourceCost cost;
			foreach (ResourceType item in map.resourceManager.ResourceTypes)
			{
				if (!currentResourceTypes.TryGetValue (item, out typeUI))
				{
					typeUI = getResourceTypeUI ();
					currentResourceTypes.Add (item, typeUI);
				}

				if (control.currentBuildingType != null) cost = control.currentBuildingType.buildCost.Find (x => x.key == item.key);
				else cost = null;

				typeUI.Show (item, map.resourceManager.GetResourceCount (item), cost?.count ?? 0, showNames);
			}
		}
	}

	public void Clear ()
	{
		if (currentResourceTypes.Count > 0)
		{
			foreach (KeyValuePair<ResourceType, ResourceTypeUI> item in currentResourceTypes)
			{
				item.Value.Hide ();
				resourceTypePool.Enqueue (item.Value);
			}
			currentResourceTypes.Clear ();
		}
	}

	private ResourceTypeUI getResourceTypeUI ()
	{
		if (resourceTypePool.Count > 0) return resourceTypePool.Dequeue ();
		return Instantiate (resourceUIPrefab, transform, false);
	}
}
