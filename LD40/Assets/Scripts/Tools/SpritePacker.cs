﻿//#define SHOW_PACKER_TEXTURE

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpritePacker
{
	private class FloatRectangle
	{
		public float left;
		public float right;

		public float bottom;
		public float top;

		public float width
		{
			get
			{
				return right - left + 1;
			}
		}

		public float height
		{
			get
			{
				return top - bottom + 1;
			}
		}

		internal FloatRectangle (float left, float right, float bottom, float top)
		{
			this.left = left;
			this.right = right;

			this.bottom = bottom;
			this.top = top;
		}

		internal FloatRectangle (Rect rect)
		{
			this.left = rect.xMin;
			this.right = rect.xMax;

			this.bottom = rect.yMin;
			this.top = rect.yMax;
		}
	}

	private class Rectangle
	{
		public int left { get; private set; }
		public int right { get; private set; }

		public int bottom { get; private set; }
		public int top { get; private set; }

		public int width
		{
			get
			{
				return right - left + 1;
			}
		}

		public int height
		{
			get
			{
				return top - bottom + 1;
			}
		}

		internal Rectangle (int left, int right, int bottom, int top)
		{
			this.left = left;
			this.right = right;

			this.bottom = bottom;
			this.top = top;
		}
	}

	private class Node
	{
		internal Node child_0;
		internal Node child_1;

		internal Rectangle rect;

		internal bool hasSprite = false;

		internal Node Insert (Rectangle spriteRect, int packingPixels)
		{
			Node tmp;

			if (child_0 != null && child_1 != null)
			{
				if ((tmp = child_0.Insert (spriteRect, packingPixels)) != null)
				{
					// sprite can be placed in child 0 return the node that the sprite has been added to
					return tmp;
				}
				else if ((tmp = child_1.Insert (spriteRect, packingPixels)) != null)
				{
					// sprite can be placed in child 1 return the node that the sprite has been added to
					return tmp;
				}

				// As this node was not a leaf and neither child could insert the sprite then sprite cant fit in this branch of the tree
				return null;
			}
			else
			{
				if (hasSprite)
				{
					// This node has already had a sprite set, and as it is a leaf, the sprite must have fit perfectly
					return null;
				}

				if (rectFitsPerfectly (spriteRect, packingPixels))
				{
					hasSprite = true;
					// This node is the exact size of the sprite being added
					return this;
				}

				if (rectFits (spriteRect, packingPixels))
				{
					// The sprite can fit in this node, so it needes to be split into the two children
					child_0 = new Node ();
					child_1 = new Node ();

					int dw = rect.width - spriteRect.width - packingPixels;
					int dh = rect.height - spriteRect.height - packingPixels;

					if (dw > dh)
					{
						child_0.rect = new Rectangle (
							rect.left,
							rect.left + spriteRect.width - 1 + packingPixels,
							rect.bottom,
							rect.top);

						child_1.rect = new Rectangle (
							rect.left + spriteRect.width + packingPixels,
							rect.right,
							rect.bottom,
							rect.top);
					}
					else
					{
						child_0.rect = new Rectangle (
							rect.left,
							rect.right,
							rect.bottom,
							rect.bottom + spriteRect.height - 1 + packingPixels);

						child_1.rect = new Rectangle (
							rect.left,
							rect.right,
							rect.bottom + +spriteRect.height + packingPixels,
							rect.top);
					}

					return child_0.Insert (spriteRect, packingPixels);
				}
				else
				{
					// The sprite cant fit in this node
					return null;
				}
			}
		}

		private bool rectFitsPerfectly (Rectangle spriteRect, int packingPixels)
		{
			//Using a packing of 1 and a rect of 4x2
			// . is empty, + is packing # is sprite
			//........
			//.++++++.
			//.+####+.
			//.+####+.
			//.++++++.
			//........

			return rect.width == spriteRect.width + packingPixels &&
				rect.height == spriteRect.height + packingPixels;
		}

		private bool rectFits (Rectangle spriteRect, int packingPixels)
		{
			//Using a packing of 1 and a rect of 4x2
			// . is empty, + is packing # is sprite
			//........
			//.++++++.
			//.+####+.
			//.+####+.
			//.++++++.
			//........

			return rect.width >= spriteRect.width + packingPixels &&
				rect.height >= spriteRect.height + packingPixels;
		}
	}

	public readonly int width;
	public readonly int height;
	public readonly int pixelBorder;

	private Node rootNode;

	private Texture2D texture;
	
#if SHOW_PACKER_TEXTURE
		private SpriteRenderer sr;
#endif
	
	public SpritePacker (int maxWidth, int maxHeight, int pixelBorder)
	{
		this.width = maxWidth;
		this.height = maxHeight;
		this.pixelBorder = pixelBorder;

		texture = new Texture2D (maxWidth, maxHeight);

		for (int x = 0; x < maxWidth; x++)
		{
			for (int y = 0; y < maxHeight; y++)
			{
				texture.SetPixel (x, y, Color.magenta);
			}
		}

		rootNode = new Node ();
		rootNode.rect = new Rectangle (0, maxWidth - 1, 0, maxHeight - 1);
	}

	/// <summary>
	/// Gets the texture from the sprite packer
	/// </summary>
	/// <returns></returns>
	public Texture2D GetTexture ()
	{
		return texture;
	}

	public Rect GetPositionRect (Rect pixlePositon)
	{
		return new Rect (
				pixlePositon.xMin / texture.width,
				pixlePositon.yMin / texture.height,
				pixlePositon.width / texture.width,
				pixlePositon.height / texture.height);
	}

	public bool AddSprite (Sprite sprite, out Rect spriteSheetPosition)
	{
		return AddSprite (sprite.texture, sprite.rect, out spriteSheetPosition);
	}

	public bool AddSprite (Texture2D spriteTex, Rect spriteRect, out Rect spriteSheetPosition)
	{
		// Set the output even if the insert fails
		spriteSheetPosition = new Rect ();

		// Get the node that will hold the sprite
		Node insertedNode = rootNode.Insert (new Rectangle ((int)spriteRect.xMin, (int)spriteRect.xMax - 1, (int)spriteRect.yMin, (int)spriteRect.yMax - 1), pixelBorder * 2);

		// If the node is null then it couldnt be added to the sprite sheet
		if (insertedNode == null) return false;

		// Otherwise, the sprite needes to be moved over to the sprite sheet

		int startX = insertedNode.rect.left;
		//int endX = insertedNode.rect.right;

		int startY = insertedNode.rect.bottom;
		//int endY = insertedNode.rect.top;

		int width = (int)spriteRect.width;
		int height = (int)spriteRect.height;

		for (int i = 0; i < pixelBorder; i++)
		{
			for (int x = 0; x < spriteRect.width; x++)
			{
				texture.SetPixel (startX + pixelBorder + x, startY + i, spriteTex.GetPixel ((int)spriteRect.xMin + x, (int)spriteRect.yMin));
				texture.SetPixel (startX + pixelBorder + x, startY + pixelBorder + height + i, spriteTex.GetPixel ((int)spriteRect.xMin + x, (int)spriteRect.yMax - 1));
			}

			for (int y = 0; y < spriteRect.height; y++)
			{
				texture.SetPixel (startX + i, startY + pixelBorder + y, spriteTex.GetPixel ((int)spriteRect.xMin, (int)spriteRect.yMin + y));
				texture.SetPixel (startX + pixelBorder + width + i, startY + pixelBorder + y, spriteTex.GetPixel ((int)spriteRect.xMax - 1, (int)spriteRect.yMin + y));
			}

			for (int a = 0; a < pixelBorder; a++)
			{
				texture.SetPixel (startX + i, startY + a, spriteTex.GetPixel ((int)spriteRect.xMin, (int)spriteRect.yMin));
				texture.SetPixel (startX + i, startY + height + pixelBorder + a, spriteTex.GetPixel ((int)spriteRect.xMin, (int)spriteRect.yMax - 1));
				texture.SetPixel (startX + width + pixelBorder + i, startY + height + pixelBorder + a, spriteTex.GetPixel ((int)spriteRect.xMax - 1, (int)spriteRect.yMax - 1));
				texture.SetPixel (startX + width + pixelBorder + i, startY + a, spriteTex.GetPixel ((int)spriteRect.xMax - 1, (int)spriteRect.yMin));
			}
		}

		int xMin = (int)spriteRect.xMin;
		int yMin = (int)spriteRect.yMin;

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				texture.SetPixel (startX + pixelBorder + x, startY + pixelBorder + y, spriteTex.GetPixel (xMin + x, yMin + y));
			}
		}

		//texture.SetPixels (
		//	startX + pixelBorder,
		//	startY + pixelBorder,
		//	(int)spriteRect.width,
		//	(int)spriteRect.height,
		//	getPixel.GetPixels (
		//		(int)spriteRect.xMin,
		//		(int)spriteRect.yMin,
		//		(int)spriteRect.width,
		//		(int)spriteRect.height));

		spriteSheetPosition = new Rect (startX + pixelBorder, startY + pixelBorder, spriteRect.width, spriteRect.height);

		texture.Apply ();

#if SHOW_PACKER_TEXTURE
			if (sr == null)
			{
				sr = new GameObject ("SpritePacker").AddComponent<SpriteRenderer> ();
				sr.transform.position = new Vector3 (-10, 0, 0);
			}

			sr.sprite = Sprite.Create (texture, new Rect (0, 0, texture.width, texture.height), Vector2.zero);
#endif

		return true;
	}
}