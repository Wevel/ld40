﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifierManager 
{
	private Dictionary<string, float> modifiers = new Dictionary<string, float> ();

	public float GetModifier (string key)
	{
		float value = 0;
		modifiers.TryGetValue (key, out value);
		return value;
	}

	public void AddModifier (Modifier modifier)
	{
		AddModifier (modifier.key, modifier.value);
	}

	public void AddModifier (string key, float value)
	{
		float currentValue;
		if (modifiers.TryGetValue (key, out currentValue)) modifiers[key] = currentValue + value;
		else modifiers[key] = value;
	}
}
